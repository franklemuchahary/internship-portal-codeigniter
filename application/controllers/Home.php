<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->library('auth');
		$data = [];
		if($this->auth->checkLogin()){
			$data['isLogin'] = true;
		}
		$this->load->core_view_loader_template('core/home_view',$data);
	}

	public function test()
	{
		$this->db->select('*');
		$this->db->from('student_resume');
		$this->db->join('student_login', 'student_login.id=student_resume.student_id');
		$q = $this->db->get();

		require_once(APPPATH.'third_party/fpdf/fpdf.php');
		$pdf = new FPDF();
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',24);
		$pdf->Cell(190,10,'RESUME',0,0,'C');
		$pdf->Ln(3);
		foreach($q->result() as $row){
			$pdf->SetFont('Arial','',12);
			$pdf->Cell(5,30,$row->fname,0,0,'L');
			$pdf->Cell(10,30, $row->lname);
			$pdf->Cell(10,50, $row->phone,0,0,'L');
			$pdf->Cell(10,70, $row->email,0,0,'L');
		}
		

		$pdf->Output();
	}
}
