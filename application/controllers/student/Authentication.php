<?php
class Authentication extends CI_Controller{

	const PATH_HOME = 'student/home';
	const PATH_LOGIN = 'student/login';
	const LOGIN_VIEW = 'student/login';
	const HOME_VIEW = 'student/home';
	const SIGNUP_VIEW = 'student/signup';
	const VIEW_ACTIVATE = 'student/activate';

	public function __construct(){
		parent::__construct();
	}

	public function showLogin(){
		$this->redirectForLogin();
		$this->viewWithHeadFootNav([self::LOGIN_VIEW]);
	}

	protected function redirectForLogin(){
		$this->load->library('auth');
		if($this->auth->checkLogin()){
			$this->load->helper('url');
			redirect(self::PATH_HOME,'location');
		}
	}

	public function showSignup(){
		$this->redirectForLogin();
		$this->viewWithHeadFootNav([self::SIGNUP_VIEW]);
	}

	public function logout(){
		$this->load->library('auth');
		$this->load->helper('url');
		$this->auth->logout();
		redirect(self::PATH_LOGIN,'location');
	}

	public function login(){
		if(!$this->validateLogin()){
			$this->viewWithHeadFootNav([self::LOGIN_VIEW]);
			return;
		}
		//process for username and password verification
		$this->load->model('student/student_details');
		try{
			if(!$this->student_details->login()){
				throw new Exception('Login Not Initiated');
			}
		}catch(Exception $e){
			//logged in unsuccesfully show error message
			$data['errorInvalid'] = true;
			$this->viewWithHeadFootNav([self::LOGIN_VIEW],$data);
			return;
		}
		//if logged in succesfully enter in application
		$this->load->helper('url');
		redirect(self::HOME_VIEW,'location');
	}

	public function signup(){
		if(!$this->validateSignup()){
			$this->viewWithHeadFootNav([self::SIGNUP_VIEW]);
			return;
		}
		$this->load->model('student/student_details');
		try{
			$response = $this->student_details->newStudent();
		}catch(Exception $e){
			//if exception occurs then redirect to form to re-enter credentials
			if(strstr($e->getMessage(),'already existing') !== false){
				$this->viewWithHeadFootNav([self::SIGNUP_VIEW],['exists' => true]);
				return;	
			}
			$this->viewWithHeadFootNav([self::SIGNUP_VIEW],['failed' => true]);
			return;
		}
		//on successful signup log the user in!
		$this->load->helper('url');
		redirect(self::HOME_VIEW);
	}

	public function activateAccount($token){
		$this->load->model('student/student_details','student');
		//check for appropriate length of token
		$data['title'] = 'SIP Activation';
		try{

			$token = substr(urldecode(base64_decode($token)),0,32);
			if(!$token){
				throw new Exception('Invalid Token');
			}
			$success = (strlen($token) === 32) ? $this->student->activateAccount($token): false;
			$data['status'] = true;
		}catch(Exception $e){
			//TODO : if token failed -> show invalid token page
			$data['status'] = false;
		}		
		$this->viewWithHeadFootNav([self::VIEW_ACTIVATE],$data);
	}

	protected function validateLogin(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->getRulesLogin());
		return $this->form_validation->run();
	}

	private function getRulesLogin(){
		return [
					[
						'field' => 'username',
						'label' => 'Username/Email',
						'rules' => 'required|valid_email'
					],
					[
						'field' => 'spassword',
						'label' => 'Password',
						'rules' => 'required|min_length[8]'
					]
				];
	}

	private function getRulesSignup(){
		return [
			[
				'field' => 'semail',
				'label' => 'Username/Email',
				'rules' => 'required|valid_email|is_unique[student_login.email]'
			],
			[
				'field' => 'sname',
				'label' => 'Name',
				'rules' => 'required|alpha_numeric_spaces'
			],
			[
				'field' => 'sphone',
				'label' => 'Phone',
				'rules' => 'required|min_length[8]|max_length[15]|numeric'
			],
			[
				'field' => 'sroll',
				'label' => 'DTU Roll No',
				'rules' => 'regex_match[/2k\d{2}\/(\w{2}|\w{3})\/(\d{4}|\d{3})/i]'
			],
			[
				'field' => 'spassword',
				'label' => 'Password',
				'rules' => 'required|min_length[8]|max_length[50]'
			],
			[
				'field' => 'srepassword',
				'label' => 'Confirm Password',
				'rules' => 'required|min_length[8]|max_length[50]|matches[spassword]'
			]
		];
	}

	public function validateSignup(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->getRulesSignup());
		return $this->form_validation->run();
	}

	private function viewWithHeadFootNav($views = [],$vars = []){
		$this->load->view('templates/general/header',$vars);
		$this->load->view('templates/general/nav',$vars);
		foreach ($views as $view) {
			$this->load->view($view,$vars);
		}
		$this->load->view('templates/general/footer',$vars);
	}
}