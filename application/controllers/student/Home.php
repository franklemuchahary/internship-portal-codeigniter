<?php

class Home extends MY_Controller{

	protected $cv;

	static protected $defaultTitle = 'Sip - Student Portal';

	const VIEW_CVBUILDER = 'student/cvbuilder';
	const VIEW_HOME = 'student/home';
	const VIEW_CHANGEPASSWORD = 'student/change_password';
	const VIEW_PROFILE = 'student/profile';

	const PATH_CVBUILDER = 'student/cvbuilder';
	const PATH_ACTIVE = 'student/activate';
	const PATH_HOME = 'student/home';

	static protected $excludedUri = [
								'student/activate',
								'student\/activate\/resend\/[^\/]*',
								'student/cvbuilder'
							];

	public function __construct(){
		parent::__construct();
		$this->load->model('student/resume');
		$this->load->model('student/student_details');
		$this->load->helper('url');
	}

	public function ifActiveRedirect(){
		if($this->user->active == Student_details::ACTIVATED){
			//user is activated already no need for this controller
			redirect(self::PATH_HOME);
		}
	}

	public function ifNotActiveRedirect(){
		if($this->user->active != Student_details::ACTIVATED){
			//user is activated already no need for this controller
			redirect(self::PATH_ACTIVE);
		}
	}

	public function activateView(){
		$this->ifActiveRedirect();
		//get messages from session
		$data = [];
		if(isset($this->session->msg)){
			$data['msg'] = $this->session->msg;
		}
		//TODO : improve logic here user some kind of other token
		$this->load->library('encryption');
		$data['token'] = urlencode(base64_encode($this->encryption->encrypt($this->user->token)));
		$this->viewWithHeadFootNav([self::PATH_ACTIVE],$data);
	}

	public function resendMail(){
		$this->ifActiveRedirect();
		try{
			$matches = [];
			$token = @preg_match('/student\/activate\/resend\/([^\/]*)/',uri_string(),$matches);
			$token = $matches[1];
			$this->load->library('encryption');
			$token = $this->encryption->decrypt(base64_decode(urldecode($token)));
			if(!$token){
				throw new Exception('Invalid Token');
			}
			$response = $this->student_details->resendMailForUser($token);
			if(!$response){
				throw new Exception("Mail Not Sent");
			}
		}catch(Exception $e){
			//error sending mail -> try again
			//TODO : log what error
			///TODO : move to a separate class flashing and retrieving errors!!
			$this->session->msg = "mail fail";
			$this->session->mark_as_flash('msg');
		}
		$this->session->msg = "mail success";
		$this->session->mark_as_flash('msg');
		redirect(self::PATH_ACTIVE);
	}

	public function index(){
		//show homepage
		$data['msg'] = $this->getMessageFromSession();
		$this->viewWithHeadFootNav([self::VIEW_HOME],$data);
	}

	public function showBuilder(){
		$this->ifNotActiveRedirect();
		if($this->isResumeComplete()){
			$this->viewResume();
			return;
		}
		$data['msg'] = $this->getMessageFromSession();
		$data['title'] = 'Cv Builder';
		$data = array_merge($data,$this->getUserDetails());
		$this->viewWithHeadFootNav([self::VIEW_CVBUILDER],$data);
	}

	public function processBuilder(){
		if(!$this->validateResume()){
			return;
		}
		//when validates;
		try{
			$this->resume->addResumeForUserId($this->user->id);
		}catch(Exception $e){
			//repopulate the resume and tell them to try again later!
			$data['fail'] = true;
			$data['title'] = 'SIP : Resume Builder';
			$data = array_merge($data,$this->getUserDetails());
			$this->viewWithHeadFootNav([self::VIEW_CVBUILDER],$data);
		}
		$this->setSessionMessage('Resume Builder Updated Successfully');
		redirect(self::PATH_HOME);
	}

	//used to show resume of the user
	public function viewResume(){
		$this->load->model('resume');
		$data = $this->resume->getResumeByUserId($this->user->id,false);
		$data['sdob'] = strftime('%d-%m-%Y',strtotime($data['sdob']));
		$this->setDataToLoadByForm($data);
		$data['header']['title'] = 'SIP : Resume Builder';
		$data['viewEdit'] = true;
		$data = array_merge($data,$this->getUserDetails());
		$this->viewWithHeadFootNav([self::VIEW_CVBUILDER],$data);
	}

	/**
	*	gets data to update directly
	*/
	public function updateResume(){
		if(!$this->validateResume()){
			return;
		}
		//when validates;
		try{
			$this->resume->updateResumeForId($this->user->id);
		}catch(Exception $e){
			//repopulate the resume and tell them to try again later!
			$data['fail'] = true;
			$data['title'] = 'SIP : Resume Builder';
			$this->viewWithHeadFootNav([self::VIEW_CVBUILDER],$data);
		}
		$this->setSessionMessage('Resume Builder Updated Successfully');
		redirect(self::PATH_HOME);
	}

	protected function validateResume(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->getResumeValidationRules());
		if($this->form_validation->run() == false){
			$data['validation'] = false;
			$data['title'] = 'SIP : Resume Builder';
			$data = array_merge($data,$this->getUserDetails());
			$this->viewWithHeadFootNav([self::VIEW_CVBUILDER],$data);
			return false;
		}
		return true;
	}

	protected function getUserDetails(){
		$data = [];
		$data['user_email'] = $this->user->email;
		$data['user_phone'] = $this->user->phone;
		$data['user_roll'] = $this->user->rollno;
		return $data;
	}

	public function viewChangePassword(){
		$data['header']['title'] = 'SIP : Change Password';
		$this->viewWithHeadFootNav([self::VIEW_CHANGEPASSWORD],$data);
	}

	public function changePassword(){
		$data['header']['title'] = 'SIP :Change Password';
		if(!$this->validatePassword()){
			return;
		}
		$this->load->model('student/student_details','student');
		try{
			$this->student->changePassword($this->user);
		}catch(Exception $e){
			if(strstr($e->getMessage(), 'Incorrect Old Password')	 !== false){
				//load incorrect password view

				$data['error'] = 'You Gave us Incorrect Old Password';

				$this->viewWithHeadFootNav([self::VIEW_CHANGEPASSWORD],$data);
				return;
			}
		}
		$this->setSessionMessage('Password was changed succesfully.');
		redirect(self::PATH_HOME);
	}

	protected function validatePassword(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->getPasswordRules());
		if($this->form_validation->run() == false){
			$data['header']['title'] = 'SIP : Change Password';
			$data['fail'] = true;
			$this->viewWithHeadFootNav([self::VIEW_CHANGEPASSWORD],$data);
		}
		return true;
	}

	public function viewProfile(){
		$data['header']['title'] = 'SIP : Profile';
		$data = array_merge($data,$this->mapUserToProfile());
		$this->setDataToLoadByForm($data['user']);
		$this->viewWithHeadFootNav([self::VIEW_PROFILE],$data);
	}

	protected function mapUserToProfile(){
		$map = [
			'name' => 'sname',
			'phone' => 'sphone',
			'email' => 'semail',
			'rollno' => 'sroll'
		];
		$data = (array)$this->user;
		$prep = [];
		foreach($map as $table => $field){
			if(isset($data[$table])){
				$prep['user'][$field] = $data[$table];
			}
		}
		return $prep;
	}

	public function updateProfile(){
		if(!$this->validateProfile()){
			return;
		}
		//check for unique email and rollno
		$this->load->model('student_details','student');
		$msg = 'Profile was Successfully Updated';
		try{
			$this->student->updateProfileByUser((array)$this->user);
		}catch(Exception $e){
			if(strstr($e->getMessage(),'email') !== false){
				$msg="That email address is already occupied";
			}elseif(strstr($e->getMessage(),'rollno')  !== false){
				$msg="That Roll No is already occupied";
			}
		}
		$this->setSessionMessage($msg);
		redirect(self::PATH_HOME,'location');
	}

	protected function validateProfile(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->getProfileRules());
		if($this->form_validation->run() === false){
			$data['header']['title'] = 'SIP : Profile';
			$this->viewWithHeadFootNav([self::VIEW_PROFILE],$data);
			return false;
		}
		return true;
	}
	/**
	* TODO:Inherited from Authentication so move to a library
	*/
	private function getProfileRules(){
		return [
			[
				'field' => 'semail',
				'label' => 'Username/Email',
				'rules' => 'required|valid_email'
			],
			[
				'field' => 'sname',
				'label' => 'Name',
				'rules' => 'required|alpha_numeric_spaces'
			],
			[
				'field' => 'sphone',
				'label' => 'Phone',
				'rules' => 'required|min_length[8]|max_length[15]|numeric'
			],
			[
				'field' => 'sroll',
				'label' => 'DTU Roll No',
				'rules' => 'regex_match[/2[K]\d{2}\/(\w{2}|\w{3})\/(\d{4}|\d{3})/i]'
			],
		];
	}


	protected function setDataToLoadByForm($data){

		$this->load->library('form_validation');
		$this->form_validation->setDataForForm($data);
	}

	protected function getPasswordRules(){
		return [
			[
				'field' => 'old_password',
				'label' => 'Old Password',
				'rules' => 'required|min_length[8]|max_length[50]'
			],
			[
				'field' => 'new_password',
				'label' => 'New Password',
				'rules' => 'required|min_length[8]|max_length[50]'
			],
			[
				'field' => 'confirm_password',
				'label' => 'Confirm Password',
				'rules' => 'required|min_length[8]|max_length[50]|matches[new_password]'
			]
		];
	}

	protected function getResumeValidationRules(){
		return [
			[
				'field' => 'sfname',
				'label' => 'First Name',
				'rules' => 'required|alpha_numeric_spaces'
			],
			[
				'field' => 'slname',
				'label' => 'Last Name',
				'rules' => 'required|alpha_numeric_spaces'
			],
			[
				'field' => 'address',
				'label' => 'Address',
				'rules' => 'required'
			],
			[
				'field' => 'sdob',
				'label' => 'Date Of Birth',
				'rules' => 'required|trim|regex_match[/(\d{2}|\d{1})-\d{2}-\d{4}/]'
			],
			[
				'field' => 'schoolname10',
				'label' => '10th School Name',
				'rules' => 'required'
			],
			[
				'field' => 'schoolboard10',
				'label' => '10th Board',
				'rules' => 'required'
			],
			[
				'field' => 'result10',
				'label' => '10th Result',
				'rules' => 'required'
			],
			[
				'field' => 'year10',
				'label' => '10th Passing Year',
				'rules' => 'required|numeric|less_than_equal_to['.(date('Y') - 1).']|greater_than_equal_to[2009]'
			],
			[
				'field' => 'schoolname12',
				'label' => '12th School Name',
				'rules' => 'required'
			],
			[
				'field' => 'schoolboard12',
				'label' => '12th Board',
				'rules' => 'required'
			],
			[
				'field' => 'result12',
				'label' => '12th Result',
				'rules' => 'required'
			],
			[
				'field' => 'year12',
				'label' => '12th Passing Year',
				'rules' => 'required|numeric|less_than_equal_to['.date('Y').']|greater_than_equal_to[2010]'
			],
			[
				'field' => 'jeemainsr',
				'label' => 'JEE Main Rank',
				'rules' => 'required|numeric'
			],
			[
				'field' => 'jeeadvsr',
				'label' => 'JEE Advance Rank',
				'rules' => 'numeric'
			],
			[
				'field' => 'pname[]',
				'label' => 'Project Name',
				'rules' => 'alpha_numeric_spaces'
			],
			// [
			// 	'field' => 'prole[]',
			// 	'label' => 'Role In Project',
			// 	'rules' => ''
			// ],
			[
				'field' => 'pyear[]',
				'label' => 'Project Year',
				'rules' => 'regex_match[/\d{4}/]|less_than_equal_to['.date('Y').']|greater_than_equal_to[2010]'
			],
//			[
//				'field' => 'pduration[]',
//				'label' => 'Project Duration'
//			],
//			[
//				'field' => 'pdesc[]',
//				'label' => 'Project Description',
//				'rules' => 'alpha_numeric_spaces'
//			],
			// [
			// 	'field' => 'technical'
			// ],
			// [
			// 	'field' => 'nontechnical'
			// ],
			[
				'field' => 'interest',
				'label' => 'Interest',
				'rules' => 'required'
			],
			// [
			// 	'field' => 'courses',
			// ],
			// [
			// 	'field' => 'achieve'
			// ],
			// [
			// 	'field' => 'cocurr'
			// ],
			// [
			// 	'field' => 'volunt'
			// ]
		];
	}

}