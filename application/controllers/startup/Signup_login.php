<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup_login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->core_view_loader_template('startup/startup_signup_view');
	}


	public function startup_signup()
	{
		$this->load->library('form_validation');
		$this->load->model('startup_models/startup_signup_login_model');
		$this->form_validation->set_rules('startup_full_name', 'Name', 'trim|required');
		$this->form_validation->set_rules('startup_phone', 'Phone', 'trim|required');
		$this->form_validation->set_rules('startup_email', 'Email', 'trim|required|valid_email|is_unique[startups_users.startup_email]');
		$this->form_validation->set_rules('startup_password', 'Password', 'trim|required|min_length[4]|max_length[32]');
		$this->form_validation->set_rules('startup_password_repeat', 'Repeat Password', 'trim|required|matches[startup_password]');
		
		if($this->form_validation->run()==false):
			$this->index();
		else:
			$add_new_startup = $this->startup_signup_login_model->create_new_startup_user_db();

			if($add_new_startup==true):
				$data['account_created'] = 'Your account has been created! <br> Please go to your inbox and verify your E-Mail ID to continue!';
				$this->load->core_view_loader_template('startup/startup_login_view', $data);
			else:
				$data['error_creating_account'] = 'Error Creating Account! <br> Please try again or notify admin!';
				$this->load->core_view_loader_template('startup/startup_signup_view', $data);
			endif;
		endif;
	}


	//receiving the verification_link_clicked
	public function account_verification($email_id, $verification_hash){
		$this->load->model('startup_models/startup_signup_login_model');
		$result = $this->startup_signup_login_model->verification_email($email_id, $verification_hash);
		$result1 = $this->startup_signup_login_model->check_if_already_verified($email_id, $verification_hash);
		
		if($result && $result1){
			$result2 = $this->startup_signup_login_model->update_verification_hash($email_id, $verification_hash);
			if($result2){
				require_once(APPPATH.'third_party/alert.php');
				echo '<script>$(window).load(function(){swal({title: "Verification Successful!", text: "You can now login!", type: "success", timer: 2000, showConfirmButton: false});});</script>'; 
				header("Refresh:2;url=".base_url()."startup/signup_login/login");
			}
			else{
				echo '<script>alert("Verification Error!")</script>';
				redirect('startup/signup_login', 'refresh');
			}
		}
		else{
			require_once(APPPATH.'third_party/alert.php');
			echo '<script>$(window).load(function(){swal({title: "Check If Already Verified!", text: "Please Try Loggin In!", type: "error", timer: 2000, showConfirmButton: false});});</script>'; 
			header("Refresh:2;url=".base_url()."startup/signup_login/login");
		}	
	}


	//login view
	public function login()
	{
		$this->load->core_view_loader_template('startup/startup_login_view');
	}


	//this method will handle the admin logins
	public function startup_login_validate()
	{
		$this->load->library('form_validation');
		$this->load->model('startup_models/startup_signup_login_model');
		$startup_email = $this->input->post('startup_login_email', true);

		$this->form_validation->set_rules('startup_login_email', 'Startup Email', 'trim|required');
		$this->form_validation->set_rules('startup_login_password', 'Startup Password', 'trim|required|callback__check_startup_password');

		if($this->form_validation->run() == FALSE):
			$this->login();
		else:
			$this->startup_signup_login_model->update_last_login($startup_email);

			$session_data['requested_url_startup'] = $this->session->userdata('redirect_url_startup');
			if(empty($session_data['requested_url_startup'])){
				redirect('startup/startup_home','refresh');	
			}
		else{
			redirect($session_data['requested_url_startup'], 'refresh');
		}
		endif;
	}


	public function _check_startup_password($startup_password)
	{
		$startup_email = $this->input->post('startup_login_email', true);

		$this->load->model('startup_models/startup_signup_login_model');
		$result = $this->startup_signup_login_model->validate_startup_login($startup_email, $startup_password);

		if($result):
			$sess_array_1 = array();
		foreach($result as $row){
			$sess_array_1 = array(
				'user_ip' => $_SERVER['REMOTE_ADDR'],
				'startup_email' => $row->startup_email,
				'startup_name' => $row->startup_name,
				'startup_id' => $row->id
				);
			$this->session->set_userdata('sip_startup_is_logged_in', $sess_array_1);
		}
		return true;
		else:
			$this->form_validation->set_message('_check_startup_password', 'Invalid Username or Password');
			return false;
		endif;
	}

}
