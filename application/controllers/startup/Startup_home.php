<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Startup_home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('sip_startup_is_logged_in')){
			$this->session->set_userdata('redirect_url_startup', current_url());
			redirect('startup/signup_login/login', 'refresh');
		}
		else{
			$this->load->model('startup_models/startup_main_model');

			$this->load->library('form_validation');
		}
	}


	public function index()
	{
		$data['startup_details'] = $this->startup_main_model->get_startup_details();
		$this->load->startup_view_loader_template('startup/startup_home_view', $data);
	}


	function update_startup_info()
	{
		$this->form_validation->set_rules('startup_name', 'Startup Name', 'trim|required');
		$this->form_validation->set_rules('startup_about', 'About Startup', 'trim|required');
		$this->form_validation->set_rules('startup_sector', 'Startup Sector', 'trim|required');
		$this->form_validation->set_rules('startup_website', 'Startup Website', 'trim');
		$this->form_validation->set_rules('startup_logo', 'Startup Logo', 'callback__do_upload');
		$this->form_validation->set_rules('contact_full_name', 'Contact Person', 'trim|required');
		$this->form_validation->set_rules('startup_phone', 'Phone', 'trim|required');
		$this->form_validation->set_rules('startup_address', 'Address', 'trim|required');
		$this->form_validation->set_rules('terms_confirm', 'Declaration', 'trim|required');

		if($this->form_validation->run() == false)
		{
			$this->index();
		}
		else
		{
			$update_data = $this->startup_main_model->update_startup_data();
			if($update_data):
				require_once(APPPATH.'third_party/alert.php');
				echo '<script>$(window).load(function(){swal({title: "Startup Details Updated!", text: "You can now post Internship Openings!", type: "success", timer: 2000, showConfirmButton: false});});</script>'; 
				header("Refresh:2;url=".base_url()."startup/startup_home/add_internship");
			else:
				require_once(APPPATH.'third_party/alert.php');
				echo '<script>$(window).load(function(){swal({title: "Error Updating Your Startup Details", text: "Sorry! Try Again", type: "error", timer: 2000, showConfirmButton: false});});</script>'; 
				header("Refresh:2;url=".base_url()."startup/secure_home");
			endif;

		}
	}

	//upload photo function
	public function _do_upload()
	{
		$config['upload_path']          = '/var/www/html/git-repos/ecell_sip/uploads/startup_logos';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 5120;

		$this->load->library('upload', $config);

		if ( !$this->upload->do_upload('startup_logo'))
		{
			$data = array('error' => $this->upload->display_errors());
			$this->form_validation->set_message('_do_upload', $data['error']);
			return true;
		}
		else
		{
			$file_name = $this->upload->data('file_name');
			$query = $this->startup_main_model->update_logo_file_name($file_name);
			if($query){
				$data = array('upload_data' => $this->upload->data());	
				return true;
			}	
		}
	}



	function add_internship()
	{
		$data['opening_details'] = $this->startup_main_model->get_startup_and_opening_details();
		$data['intern_profiles'] = $this->startup_main_model->get_intern_profiles_list();
		$data['startup_details'] = $this->startup_main_model->get_startup_details();
		$this->load->startup_view_loader_template('startup/startup_add_internship_view', $data);
	}


	function add_new_internship_opening()
	{
		$this->form_validation->set_rules('intern_profile', 'Profile', 'trim|required');
		$this->form_validation->set_rules('intern_type', 'Internship Type', 'trim|required');
		$this->form_validation->set_rules('intern_descp', 'Internship Description', 'trim|required');
		$this->form_validation->set_rules('intern_stipend_type', 'Stipend Type', 'trim|required');
		$this->form_validation->set_rules('intern_stipend', 'Internship Stipend', 'trim|numeric|required');
		$this->form_validation->set_rules('intern_duration', 'Internship Duration', 'trim|required');
		$this->form_validation->set_rules('intern_location', 'Internship Location', 'trim|required');
		$this->form_validation->set_rules('intern_deadline', 'Application Deadline', 'trim|required');

		if($this->form_validation->run() == false)
		{
			$this->add_internship();
		}
		else
		{
			if($this->startup_main_model->add_intern_opening_data()):
				require_once(APPPATH.'third_party/alert.php');
				echo '<script>$(window).load(function(){swal({title: "Scroll Down To See Your Openings!", text: "Please expect a confirmation call from us shortly.", type: "success", timer: 2000, showConfirmButton: false});});</script>'; 
				header("Refresh:2.5;url=".base_url()."startup/startup_home/add_internship");
			else:
				require_once(APPPATH.'third_party/alert.php');
				echo '<script>$(window).load(function(){swal({title: "Error Updating Your Internship Opening", text: "Sorry! Try Again", type: "error", timer: 2000, showConfirmButton: false});});</script>'; 
				header("Refresh:2;url=".base_url()."startup/secure_home");
			endif;

		}
	}


	function applications_received($profile_id)
	{
		try{
			$data['applics'] = $this->startup_main_model->get_applications_data($profile_id);
			$data['stats'] = $this->startup_main_model->get_status();
		}
		catch(Exception $e){
			show_404();
		}
		$this->load->startup_view_loader_template('startup/startup_applications_received', $data);
	}


	function student_more_details($student_id)
	{
		try{
			$data['stud_det'] = $this->startup_main_model->get_applicant_details($student_id);
			$data['stud_proj'] = $this->startup_main_model->get_applicant_projects($student_id);
		}
		catch(Exception $e){
			show_404();
		}
		$this->load->startup_view_loader_template('startup/student_details', $data);
	}


	function update_applications()
	{
		$id = $this->input->post('profile_id', true);
		try{
			$update = $this->startup_main_model->update_applic_status($id);
			$success = true;
		}
		catch(Exception $e){
			$success = false;	
		}

		if($success == true):
			require_once(APPPATH.'third_party/alert.php');
			echo '<script>$(window).load(function(){swal({title: "Application Statuses Updated", type: "success", timer: 2000, showConfirmButton: false});});</script>'; 
			header("Refresh:1.5;url=".base_url()."startup/startup_home/applications_received/".$id);
		else:
			require_once(APPPATH.'third_party/alert.php');
			echo '<script>$(window).load(function(){swal({title: "Error Updating Application Statuses", text: "Sorry! Try Again", type: "error", timer: 2000, showConfirmButton: false});});</script>'; 
			header("Refresh:1.5;url=".base_url()."startup/startup_home/applications_received/".$id);
		endif;
	}


	function contact()
	{
		$data['contact_data'] = $this->startup_main_model->get_contact_data();
		$this->load->startup_view_loader_template('startup/startup_contact', $data);
	}

	function post_new_query()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('startup_comp_sub', 'Subject', 'trim|required');
		$this->form_validation->set_rules('startup_comp_msg', 'Message', 'trim|required');

		if($this->form_validation->run() == false):
			$this->contact();
		else:
			if($this->startup_main_model->add_new_comp()):
				require_once(APPPATH.'third_party/alert.php');
				echo '<script>$(window).load(function(){swal({title: "Scroll Down To See Your Past Queries!", type: "success", timer: 2000, showConfirmButton: false});});</script>'; 
				header("Refresh:1.5;url=".base_url()."startup/startup_home/contact");
			else:
				require_once(APPPATH.'third_party/alert.php');
				echo '<script>$(window).load(function(){swal({title: "Error Adding A Query.", text: "Sorry! Try Again", type: "error", timer: 2000, showConfirmButton: false});});</script>'; 
				header("Refresh:1.5;url=".base_url()."startup/secure_home/contact");
			endif;
		endif;
	}



}