<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emails_scheduled extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		//$this->authenticate();
	}

	function send_startup_verification_email_cron()
	{
		$this->load->model('email_model');
		$this->email_model->send_startup_account_verification_email();
		$this->email_model->sendMails();
	}


	function authenticate()
	{
		// User name and password for authentication
		$username = 'cron';
		$password = 'cron';

		if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW']) ||
			($_SERVER['PHP_AUTH_USER'] != $username) || ($_SERVER['PHP_AUTH_PW'] != $password)) {
    		// The user name/password are incorrect so send the authentication headers
			header('HTTP/1.1 401 Unauthorized');
			header('WWW-Authenticate: Basic realm="Please navigate back if you are not an administrator!"');
			exit('<h2>Error</h2>Sorry, you must enter a valid user name and password to access this page.');
		}	
	}

}