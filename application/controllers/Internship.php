<?php 
class Internship extends MY_Controller{

	const VIEW_INTERNSHIPS = 'posting/all';

	const PATH_VIEWALL = '/student/internships';
	CONST PATH_INTERNSHIPS = 'student/profile/internships';
	
	public function __construct(){
		parent::__construct();
		$this->load->model('startup_models/posting');
		$this->load->model('student/StudentInternshipLink','applied_posts');
	}
	

	public function viewAll(){
		list($posts,$company) = $this->posting->getAllPostingWithCompany($this->user->id);
		$data['header']['title'] = 'SIP : Internships';
		//$data['nav']['loggedIn'] = true;
		$data['msg'] = $this->getMessageFromSession();
		$data['posts'] = $posts;
		$data['company'] = $company;
		$data['applied'] = false;
		$this->viewWithHeadFootNav([self::VIEW_INTERNSHIPS],$data);
	}

	public function viewAppliedInterns(){
		list($posts,$companies) = $this->applied_posts->getAppliedWithCompany($this->user->id);
		$data['header']['title'] = 'SIP : Applied Internships';
		$data['msg'] = $this->getMessageFromSession();
		$data['posts'] = $posts;
		$data['company'] = $companies;
		$data['applied'] = true;
		$this->viewWithHeadFootNav([self::VIEW_INTERNSHIPS],$data);
	}

	public function apply(){
		$this->load->helper('url');
		if($this->validateApply()){
			try{
				$this->posting->apply($this->user->id);
				$this->setSessionMessage('Applied For This Internship Successfully');
			}catch(Exception $e){
				$this->setSessionMessage('Application Failed. Try Again.');
			}
		}
		redirect(self::PATH_VIEWALL,'location');
	}

	protected function validateApply(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('internship_id','Internship Identifier','required|numeric');
		return $this->form_validation->run();
	}

	public function removeApplication(){
		$this->load->helper('url');
		if($this->validateRemove()){
			try{
				$this->applied_posts->remove($this->user->id);
				$this->setSessionMessage("Your Application for this internship has been removed");
			}catch(Exception $e){
				$this->setSessionMessage('Your Internship Application Not Found ');
			}
		}
		redirect(self::PATH_INTERNSHIPS,'location');
	}

	public function validateRemove(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('internship_id','Internship Apply Identifier','required|numeric');
		return $this->form_validation->run();
	}
}