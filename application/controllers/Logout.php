<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller{

	public function startup_logout()
	{
		$this->session->unset_userdata('sip_startup_is_logged_in');
		session_destroy();
		redirect('startup/signup_login/login','refresh');
	}

}