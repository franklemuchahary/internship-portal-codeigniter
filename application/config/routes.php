<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//make a class to configure router woth prefixes perhaps?
define("STARTUP", 'startup/');
define("STUDENT",'student/');


//Routes For Student Sessions
$route['text/test'] = 'emails_scheduled/send_startup_verification_email_cron';
$route[STUDENT.'login']['GET'] = 'student/authentication/showLogin';
$route[STUDENT.'login']['POST'] = 'student/authentication/login';
$route[STUDENT.'logout'] = 'student/authentication/logout';

$route[STUDENT.'signup']['GET'] = 'student/authentication/showSignup';
$route[STUDENT.'signup']['POST'] = 'student/authentication/signup';
//student account activation
$route[STUDENT.'activate'] = 'student/home/activateView';
$route[STUDENT.'activate/resend/(:any)'] = 'student/home/resendMail';
$route[STUDENT.'activate/(:any)'] = 'student/authentication/activateAccount/$1';
//student cvbuilder
$route[STUDENT.'cvbuilder']['GET'] = 'student/home/showBuilder';
$route[STUDENT.'cvbuilder']['POST'] = 'student/home/processBuilder';
$route[STUDENT.'cvbuilder/update']['POST'] = 'student/home/updateResume';
//Student Settings
$route[STUDENT.'profile/password']['GET'] = 'student/home/viewChangePassword';
$route[STUDENT.'profile/password']['POST'] = 'student/home/changePassword';

$route[STUDENT.'profile/internships'] = 'internship/viewAppliedInterns';

$route[STUDENT.'profile']['GET'] = 'student/home/viewProfile';
$route[STUDENT.'profile']['POST'] = 'student/home/updateProfile';
//student internships
$route[STUDENT.'internships']['GET'] = 'internship/viewAll';
$route[STUDENT.'internships']['POST'] = 'internship/apply';
$route[STUDENT.'internships/remove']['POST'] = 'internship/removeApplication';

$route[STUDENT.'home'] = 'student/home/index';
//$route[STUDENT.'(:any)'] = 'student/home/index';



//Routes For Startup Sessions
$route[STARTUP.'login']['GET'] = 'startup/Signup_login/login';
$route[STARTUP.'signup']['GET'] = 'startup/Signup_login/index';

//after startups log in
$route[STARTUP.'home']['GET'] = 'startup/startup_home/index';
$route[STARTUP.'internships']['GET'] = 'startup/startup_home/add_internship';
$route[STARTUP.'applications/(:any)']['GET'] = 'startup/startup_home/applications_received/$1';
$route[STARTUP.'applications/applicants/(:any)']['GET'] = 'startup/startup_home/student_more_details/$1';
$route[STARTUP.'contact/admin']['GET'] = 'startup/startup_home/contact';


$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
