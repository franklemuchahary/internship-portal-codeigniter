<?php
//TODO : Interface -> getTableName,getById
class Auth{

	const USERNAME = 'email';
	const PASSWORD = 'password';
	const ID = 'id';

	protected $userid = 'user_id';
	protected $useremail = 'user_email';

	protected $CI;
	protected $db;
	private $session;

	protected $user = null;
	private $isLogIn = false;
	private $destroy = false;
	
	private $authModel = 'student/student_details';
	private $model;

	public function __construct($authModel = ''){
		$this->CI =& get_instance();
		$this->CI->load->database();
		$this->CI->load->library('session');
		if(!empty($authModel)){
			$this->authModel = $authModel;
		}
		$this->CI->load->model($this->authModel,'model');
		$this->db =& $this->CI->db;
		$this->session =& $this->CI->session;
		$this->model =& $this->CI->model;
	}

	public function isLoggedIn(){
		return $this->isLogIn;
	}

	public function getUser(){
		if(!empty($this->user)){
			return $this->user;
		}
		return false;
	}

	public function login($username,$password){
		$user = $this->db->get_where($this->model->getTableName(),[self::USERNAME => $username])->row();
		if(empty($user)){
			return false;
		}
		if(!$this->validatePassword($password,$user->{self::PASSWORD})){
			return false;
		}
		//TODO : check for disabled users!!

		//if validated then store user in session
		$this->CI->session->{$this->userid} = $user->{self::ID};
		$this->CI->session->{$this->useremail} = $user->{self::USERNAME};

		//instantiate the auth class as we would work on it
		$this->setLogin($user);
		return true;
	}

	private function setLogin($user){
		$this->user= $user;
		$this->isLogIn = true;
	}

	public function checkLogin($destroy = true){
		if(!isset($this->session->{$this->userid}) || !isset($this->session->{$this->useremail})){
			//destroy the session since it shouldnt exist in the first place at such conditions
			//TODO : instead add session keys to exception list
			if($destroy){
				$this->doDestroy();
			}
			return false;
		}
		if(!is_numeric($this->session->{$this->userid}) || !is_string($this->session->{$this->useremail})){
			if($destroy){
				$this->doDestroy();
			}
			return false;
		}
		//if session has both values validate them
		$user = $this->model->getUserById($this->session->{$this->userid});
		if($user->email != $this->session->{$this->useremail}){
			//mis match email and id -> possible code fuckup or hack attempt
			//TODO : log error here;
			$this->doDestroy();
			return false;
		}
		//TODO : check for disabled users!!

		//passed all checks so user is correctly logged in
		$this->setLogin($user);
		return true;
	}

	protected function validatePassword($password = '',$actualPassword = ''){
		if(password_verify($password,$actualPassword)){
			return true;
		}
		return false;
	}

	private function getPassword($password){
		//if password has null character or \n
		$password = str_replace(["\n","\0"], "", $password);
		$password = password_hash($password,PASSWORD_BCRYPT);
		if(empty($password)){
			throw new Exception("Error : Password Contains Some Off Characters");
		}
		return $password;
	}

	public function clearUserSession(){
		if(isset($this->session->{$this->userid})){
			$this->session->unset_userdata($this->userid);
		}
		if(isset($this->session->{$this->useremail})){
			$this->session->unset_userdata($this->useremail);
		}
	}

	public function logout(){
		$this->clearUserSession();
		$this->doDestroy();
	}

	public function noDestroy(){
		return $this->destroy = false;
	}

	public function doDestroy(){
		$this->clearUserSession();
		if(!empty(session_id())){
			$this->destroy = true;
			$this->session->sess_destroy();
		}
		return $this->destroy;
	}

	public function __destruct(){
		//if($this->destroy){
			//session_destroy();
		//}
	}

}