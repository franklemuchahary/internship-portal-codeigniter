<?php

class MY_Form_validation extends CI_Form_validation{
	
	public function setDataForForm($data){
		foreach($data as $key => $value){
			$this->_field_data[$key]['postdata'] = $value;
		}
	}

	public function hasDataArray($field){
		if(isset($this->_field_data[$field]['postdata'])){
			return !empty($this->_field_data[$field]['postdata']);
		}else{
			return false;
		}
	}
}