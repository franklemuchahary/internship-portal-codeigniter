<?php

class MY_Controller extends CI_Controller{

	const performInitialChecks = true;

	protected $user = null;
	protected $isLogin = false;

	//to escape these paths from uri checking
	static protected $excludedUri = [];

	//some paths to redirect to incase initial checks are failed
	const PATH_CVBUILDER = 'student/cvbuilder';
	const PATH_ACTIVE = 'student/activate';
	const PATH_HOME = 'student/home';

	static protected $loginPage = 'student/login';

	public function __construct(){
		parent::__construct();
		//check if authenticated or not
		$this->load->model('student/student_details');
		$this->authenticate();
		if(static::performInitialChecks){
			$this->performInitialChecks();
		}
	}

	public function performInitialChecks(){
		//pass initial check, for some urls
		$currentUri = uri_string();
		foreach (static::$excludedUri as $uri){
			if(@preg_match('/'.$uri.'/',$currentUri)){
				return;
			}
		}
		if(in_array(uri_string(), static::$excludedUri)){
			return;
		}
		//check if the user is activated/disabled or not?
		$this->isActive();
		//every student needs to first go through the resume builder so do that
		if(!$this->isResumeComplete()){
			redirect(self::PATH_CVBUILDER);
		}
	}

	public function isActive(){
		//student_details available as already loaded by authentication
		if($this->user->active == Student_details::DEACTIVATE){
			//since the user is deactive show them the activation page first
			//TODO : no redirect just show that view instead!
			redirect(self::PATH_ACTIVE);
		}
	}

	public function isResumeComplete(){
		$this->load->model('student/resume');
		$this->cv = $this->resume->getResumeByUserId($this->user->id);
		if(empty($this->cv)){
			return false;
		}
		return true;
	}

	protected function authenticate(){
		$this->load->library('auth');
		if(!$this->auth->checkLogin()){
			$this->redirect();
			return;
		}
		//authenticated to be logged in 
		$this->user = $this->auth->getUser();
		$this->isLogin = true;
		$this->checkIfRedirectNeeded();
	}

	protected function checkIfRedirectNeeded(){
		if(isset($this->session->nexturl)){
			$this->load->helper('url');
			$url = $this->session->nexturl;
			$this->session->unset_userdata('nexturl');
			redirect($url,'location');
		}
	}

	protected function redirect(){
		$this->load->helper('url');
		$this->session->nexturl = uri_string();
		redirect(static::$loginPage,'location');
	}

	protected function getMessageFromSession(){
		if(isset($this->session->msg)){
			return $this->session->msg;
		}else{
			return '';
		}
	}

	protected function setSessionMessage($message){
		if(isset($this->session->msg)){
			$this->session->msg .= ":;:".$message;
		}else{
			$this->session->msg = $message;
		}
		$this->session->mark_as_flash('msg');
	}

	protected function viewWithHeadFootNav($views = [],$vars = []){
		$vars = array_merge($this->getViewData(),$vars);

		$this->loadHeader($vars);

		$this->getNavigationToLoad($vars);

		foreach ($views as $view) {
			$this->load->view($view,$vars);
		}

		$this->loadFooter();
	}

	protected function getViewData(){
		//since can be accessed by login users only append isLogin
		$vars['isLogin'] = true;
		if(isset(static::$viewTitle)){
			$vars['title'] = static::$viewTitle;
		}else{
			$vars['title'] = 'Startup Internship Portal';
		}

		return $vars;
	}

	protected function loadHeader($vars = []){
		if(isset(static::$customHeader)){
			$headerView = static::$customHeader;
		}else{
			$headerView = 'templates/general/header';
		}
		if(isset($vars['header'])){
			$headerVars = $vars['header'];
		}else{
			$headerVars = $vars;
		}
		$this->load->view($headerView,$headerVars);
	}

	protected function getNavigationToLoad($decider){
		if(isset($decider['header']['msg'])){
			$headerVars['msg'] = $decider['header']['msg'];
		}elseif(isset($decider['msg'])){
			$headerVars['msg'] = $decider['msg'];
		}
		$this->load->view('templates/general/nav',$decider);
	}

	protected function loadFooter($vars = []){
		if(isset(static::$customFooter)){
			$footerView = static::$customFooter;
		}else{
			$footerView = 'templates/general/footer';
		}
		if(isset($vars['footer'])){
			$footerVars = $vars['footer'];
		}else{
			$footerVars = $vars;
		}
		$this->load->view($footerView,$footerVars);
	}


}