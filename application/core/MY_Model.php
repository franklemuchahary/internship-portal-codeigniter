<?php

class MY_Model extends CI_Model{

	static protected $timeformat = '%Y-%m-%d %H:%M:%S';

	static protected $timeformatWithoutHour = '%Y-%m-%d';

	const RETURN_ARRAY = 'array';
	const RETURN_OBJECT = 'object';

	public function __construct(){
		parent::__construct();
		$this->load->database();
		if(isset(static::$relations) && count(static::$relations) !== 0){
			foreach(static::$relations as $name => $relation){
				$this->load->model($relation['model'],$name);
			}
		}
	}

	protected function getTimeStamp($time = null){
		$time = ($time) ?: time();
		return strftime(static::$timeformat,$time);
	}

	protected function getTimeStampWithoutHour($time = null){
		$time = ($time) ?: time();
		return strftime(static::$timeformatWithoutHour,$time);
	}

	/**
	* data - email, body, subject view
	*/
	protected function sendMail($email,$subject,$view,$data){
		//helper to get base url in email
		$this->load->model('email_model');
		$this->load->helper('url');
		$email_content = $this->load->view($view,$data,true);
		$data[Email_model::EMAIL] = $email;
		$data[Email_model::BODY] = $email_content;
		$data[Email_model::SUBJECT] = $subject;
		$this->email_model->saveMail($data);
	}

	protected function assignMap($map,$data){
		$prep = [];
        foreach($map as $key => $value){
            if(is_int($key) && isset($data[$value])){
                $prep[$value] = $data[$value];
            }elseif(is_string($key) && isset($data[$value])){
                $prep[$key] = $data[$value];
            }
        }
        return $prep;
	}

	protected function getFromInput($map){
		$prep = [];
		foreach($map as $key => $value){
			if(is_int($key) && isset($data[$key])){
                $prep[$value] = $this->input->post($value);
            }elseif(is_string($key)){
                $prep[$key] = $this->input->post($value);
            }
        }
        return $prep;
	}

	protected function convertIntoMap($map,&$data){
		foreach($map as $table => $field){
			if(isset($data[$table])){
				$data[$field] = $data[$table];
				if($field != $table){
					unset($data[$table]);
				}
			}
		}
		return $data;
	}

	protected function getById($id,$returnType = self::RETURN_ARRAY){
		if(!is_numeric($id) || $id == 0){
			throw new Exception('Invalid Id Linkage Given');
		}
		$query = $this->db->get_where(static::$table,[static::ID => $id]);
		if($returnType === self::RETURN_OBJECT){
			return $query->result();
		}else{
			return $query->result_array();
		}
	}

	protected function arrangeBy($field,$list,$append = true){
		$data = [];
		$notFound = [];
		foreach($list as $item){
			if(isset($item[$field])){
				$data[$item[$field]] = $item;
			}else{
				$notFound[] = $item;
			}
		}
//		return ($append) ? array_merge($data,$notFound) : $data;
		return ($append) ? $data + $notFound : $data;
	}
}