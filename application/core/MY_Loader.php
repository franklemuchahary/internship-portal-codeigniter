<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Loader extends CI_Loader{

	public function core_view_loader_template($template_name, $vars = array(), $return=FALSE)
	{
		if($return):
			$content = $this->view('core/partials/header_view', $vars, $return);
			$content .= $this->view('templates/general/nav', $vars, $return);
			$content .= $this->view($template_name, $vars, $return);
			$content .= $this->view('core/partials/footer_view', $vars, $return);

			return $content;
		else:
			$this->view('core/partials/header_view', $vars);
			$this->view('templates/general/nav', $vars);
			$this->view($template_name, $vars);
			$this->view('core/partials/footer_view', $vars);
		endif;
	}


	public function startup_view_loader_template($template_name, $vars = array(), $return=FALSE)
	{
		if($return):
			$content = $this->view('startup/partials/header_view', $vars, $return);
			$content .= $this->view('templates/startup/nav', $vars, $return);
			$content .= $this->view($template_name, $vars, $return);
			$content .= $this->view('startup/partials/footer_view', $vars, $return);

			return $content;
		else:
			$this->view('startup/partials/header_view', $vars);
			$this->view('templates/startup/nav', $vars);
			$this->view($template_name, $vars);
			$this->view('startup/partials/footer_view', $vars);
		endif;
	}
}