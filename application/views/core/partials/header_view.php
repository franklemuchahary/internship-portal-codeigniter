<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
<head>
  <!--Import Google Icon Font-->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="/css/materialize.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="/css/style.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="/css/sip.css"  media="screen,projection"/>

  <!--Let browser know website is optimized for mobile-->
  <title>Startup Internship Portal - ECell DTU</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>