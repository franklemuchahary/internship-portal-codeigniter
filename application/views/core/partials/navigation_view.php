<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<nav>
  <div class="nav-wrapper cyan darken-3">
    <a class="brand-logo"  href="/home"><img src="/img/logo_white.png"></a>
    <ul class="right hide-on-med-and-down">
      <li><a href="/about">ABOUT</a></li>
      <!--<li><a href="/internships">INTERNSHIPS</a></li>-->
      <li><a class="dropdown-button"  data-activates="dropdown1">SIGNUP<i class="material-icons right">arrow_drop_down</i></a></li>
      <li><a href="#">CONTACT</a></li>    
    </ul>
    <!-- Dropdown Structure -->
    <ul id="dropdown1" class="dropdown-content">
      <li><a href="student/login">Student</a></li>
      <li><a href="<?php echo base_url('startup/signup_login');?>">Startup</a></li>            
    </ul>
    

    <ul id="slide-out" class="side-nav">   
      <li><a href="#"><i class="material-icons left">&#xE915;</i>ABOUT</a></li>                
      
      <li><a href="internships.html"><i class="material-icons left">&#xE168;</i>INTERNSHIPS</a></li>
      
      <li>
        <a class="dropdown-button"  data-activates="dropdown2">SIGNUP<i class="material-icons right">arrow_drop_down</i></a>
      </li>             
      
      <li><a href="#"><i class="material-icons left">&#xE8AC;</i>CONTACT</a></li>
    </ul>
    
    <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
    
  </div>
</nav>