<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style type="text/css">
 button.jo {
  background: #147190;
  color: white;
  padding: 15px 25px;
  font-size: 16px;
  border: none;
}

@media(max-width: 580px){
  button.jo{
    margin-bottom: 25px;   
    width: 100%  
  }
}

body {
  background-color: rgb(34, 42, 53);
}
</style>

<div class="container">


  <div class="row">
    <div class="s12 col center" style="margin-top:25px;margin-bottom:40px;"><img src="img/sip.png" class="responsive"></div>    

    <!-- <img src="img/redcarpetup.jpg"> -->


    <div class="col m4 s12 center">      
      <a href="<?php echo base_url('student/login');?>"><button class="jo"> STUDENT LOGIN</button></a>
    </div>         

    <div class="col m4 s12 center">       
      <a href="<?php echo base_url('student/login');?>"><button class="jo"> INTERNSHIPS</button></a>
    </div>

    <div class="col m4 s12 center">       
      <a href="<?php echo base_url('startup/login');?>"><button class="jo"> STARTUP LOGIN</button></a>
    </div>


  </div>
</div>

<section id="about">
  <div class="container">    

    <div class="row">

    <div class="s12 col center" style="margin-top:25px;margin-bottom:40px;"><img src="<?php echo base_url()?>img/what.PNG" class="responsive"></div> 

      <div class="s12 col center" style="color:white">
        <h5>The Startup Internship Portal, a one of a kind initiative which gives you the opportunity to hire skilled interns for technical and non technical profiles from various streams from one of the most prestigious colleges of India, DTU.  </h5>
      </div> 

      <div class="s12 col center" style="margin-top:25px;margin-bottom:40px;"><img src="<?php echo base_url()?>img/why.PNG" class="responsive"></div> 
      <div class="s12 col center" style="margin-top:25px;margin-bottom:40px;"><img src="<?php echo base_url()?>img/other.PNG" class="responsive"></div> 
      <div class="s12 col center" style="margin-top:25px;margin-bottom:40px;"><img src="<?php echo base_url()?>img/venn.PNG" class="responsive"></div> 
    </div>
  </div>
</section>








      <!-- <section id="partners">
        
    </section> -->
    <section id="contact">
      <div class="container">    

        <div class="row">

          <div class="s12 col center" style="color:white">
            <h3>CONTACT US</h3>
            <br><br>
            <h5> You can reach out to us at <span>sip@ecelldtu.com,9891694789</span></h5>
            <br>
            <p>All Rights Reserved , E-Cell DTU</p>
          </div> 

        </div>
      </div>
    </section>
