<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<nav>
        <div class="nav-wrapper cyan darken-3">
          <a class="brand-logo"  href="<?php echo base_url();?>"><img src="/img/logo_white.png"></a>
          
          <ul class="right hide-on-med-and-down">
            <li><a href="<?php echo base_url();?>#">ABOUT</a></li>

            <?php if($this->session->userdata('sip_startup_is_logged_in')):?>

                <li><a href="<?php echo base_url('startup/home'); ?>">STARTUP HOME</a></li> 
                <li><a href="<?php echo base_url('startup/internships'); ?>">INTERNSHIP OPENINGS</a></li>
                <li><a href="<?php echo base_url('startup/contact/admin'); ?>">QUERIES</a></li> 
                <li><a href="<?php echo base_url('logout/startup_logout'); ?>">LOGOUT</a></li>    

            <?php else: ?>
             <li><a class="dropdown-button"  data-activates="dropdown-login">Login<i class="material-icons right">arrow_drop_down</i></a>
              <!-- Dropdown Structure -->
              <ul id="dropdown-login" class="dropdown-content">
                <li><a href="/student/login">Student</a></li>
                <li><a href="/startup/login">Startup</a></li>
              </ul>
            </li>
            <li><a class="dropdown-button"  data-activates="dropdown-signup">Signup<i class="material-icons right">arrow_drop_down</i></a>
              <!-- Dropdown Structure -->
              <ul id="dropdown-signup" class="dropdown-content">
                <li><a href="/student/signup">Student</a></li>
                <li><a href="/startup/signup">Startup</a></li>            
              </ul>
            </li>
          <li><a href="#">CONTACT</a></li>   
          <?php endif ?>
        </ul>





        <ul id="slide-out" class="side-nav">   
          <li><a href="#">ABOUT</a></li>

            <?php if($this->session->userdata('sip_startup_is_logged_in')):?>

                <li><a href="<?php echo base_url('startup/home'); ?>">STARTUP HOME</a></li> 
                <li><a href="<?php echo base_url('startup/internships'); ?>">INTERNSHIP OPENINGS</a></li>
                <li><a href="<?php echo base_url('startup/contact/admin'); ?>">QUERIES</a></li> 
                <li><a href="<?php echo base_url('logout/startup_logout'); ?>">LOGOUT</a></li>    

            <?php else: ?>
             <li><a class="dropdown-button"  data-activates="dropdown-login">Login<i class="material-icons right">arrow_drop_down</i></a>
              <!-- Dropdown Structure -->
              <ul id="dropdown-login" class="dropdown-content">
                <li><a href="/student/login">Student</a></li>
                <li><a href="/startup/login">Startup</a></li>
              </ul>
            </li>
            <li><a class="dropdown-button"  data-activates="dropdown-signup">Signup<i class="material-icons right">arrow_drop_down</i></a>
              <!-- Dropdown Structure -->
              <ul id="dropdown-signup" class="dropdown-content">
                <li><a href="/student/signup">Student</a></li>
                <li><a href="/startup/signup">Startup</a></li>            
              </ul>
            </li>
          <li><a href="#">CONTACT</a></li>   
          <?php endif ?>
        </ul>
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>

      </div>
    </nav>