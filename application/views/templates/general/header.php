<html>
    <head>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="/css/materialize.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="/css/style.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="/css/sip.css"  media="screen,projection"/>
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title><?php echo isset($title) ? $title : 'SIP' ?></title>
      <?php 
          //add extra meta-tags or style tags iff required
          if(isset($metas)){
            foreach($metas as $meta){
              ?>
              <meta <?php echo "{$meta['key1']} = {$meta['value1']}";if(isset($meta['key2'])) echo "{$meta['key2']} = {$meta['value2']}"; ?> 
      <?php
            }
          }
          //to add extra links if required -> pattern - href,rel,type
          if(isset($links)){
            foreach ($links as  $link) {?>
                <link rel="<?php echo $link['rel'] ?>" type="<?php echo $link['type'] ?>" href="<?php echo $link['href'] ?>" />
      <?php }
          }
        //to print extra scripts
          if(isset($scriptsH)){
            foreach ($scriptsH as  $script) {?>
                <script src="<?php echo $script ?>"></script>
      <?php }
          }
      ?>

    </head>
    <body>
