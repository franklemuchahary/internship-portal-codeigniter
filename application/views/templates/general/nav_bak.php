       <nav>
        <div class="nav-wrapper cyan darken-3">
          <a class="brand-logo"  href="<?php echo base_url();?>"><img src="/img/logo_white.png"></a>
          <ul class="right hide-on-med-and-down">
            <li><a href="<?php echo base_url('#about')?>">ABOUT</a></li>
             <li><a href="<?php echo base_url('#contact')?>">CONTACT</a></li>    
            <?php if(isset($isLogin) && $isLogin):?>
              <li><a href="/student/internships">INTERNSHIPS</a></li>
              <li><a href="/student/cvbuilder">CV GENERATOR</a></li>
              <li><a class="dropdown-button"  data-activates="dropdown-profile">Profile<i class="material-icons right">arrow_drop_down</i></a>
                <!-- Dropdown Structure -->
                <ul id="dropdown-profile" class="dropdown-content">
                  <li><a href="/student/profile">Edit Profile</a></li>
                  <li><a href="/student/profile/password">Change Password</a></li>
                </ul>
              </li>
              <li><a href="/student/profile/internships">Applied Internships</a></li>
            <?php elseif($this->session->userdata('sip_startup_is_logged_in')):?>
              <ul class="right hide-on-med-and-down">
<<<<<<< HEAD
                  <li><a href="#">About</a></li>
                <?php if(isset($isLogin) && $isLogin):?>
                    <li><a href="/student/cvbuilder">Cv Generator</a></li>
                    <li class="dropdown-button dropdown-wide" data-activates="dropdown-interns">Internships <i class="material-icons right">arrow_drop_down</i>
                    <ul id="dropdown-interns" class="dropdown-content">
                        <li><a href="/student/internships">Available Internships</a></li>
                        <li><a href="/student/profile/internships">Applied Internships</a></li>
                    </ul>
                    </li>
                    <li><a class="dropdown-button dropdown-med"  data-activates="dropdown-profile">Profile <i class="material-icons right">arrow_drop_down</i></a>
                        <!-- Dropdown Structure -->
                        <ul id="dropdown-profile" class="dropdown-content">
                            <li><a href="/student/profile">Edit Profile</a></li>
                            <li><a href="/student/profile/password">Password</a></li>
                        </ul>
                    </li>
                    <li><a href="/student/logout">Logout</a></li>

                <?php else: ?>
                   <li><a class="dropdown-button"  data-activates="dropdown-login">Login<i class="material-icons right">arrow_drop_down</i></a>
                      <!-- Dropdown Structure -->
                      <ul id="dropdown-login" class="dropdown-content">
                        <li><a href="/student/login">Student</a></li>
                        <li><a href="/startup/login">Startup</a></li>
                      </ul>
                   </li>
                   <li><a class="dropdown-button"  data-activates="dropdown-signup">Signup<i class="material-icons right">arrow_drop_down</i></a>
                      <!-- Dropdown Structure -->
                      <ul id="dropdown-signup" class="dropdown-content">
                        <li><a href="/student/signup">Student</a></li>
                        <li><a href="/startup/signup">Startup</a></li>            
                      </ul>
                   </li>
               <?php endif ?>
                  <li><a href="#">Contact</a></li>
                </ul>
=======

                <li><a href="<?php echo base_url('startup/home'); ?>">STARTUP HOME</a></li> 
                <li><a href="<?php echo base_url('startup/internships'); ?>">INTERNSHIP OPENINGS</a></li>
                <li><a href="<?php echo base_url('startup/contact/admin'); ?>">QUERIES</a></li> 
                <li><a href="<?php echo base_url('logout/startup_logout'); ?>">LOGOUT</a></li>    
              </ul>
>>>>>>> ee136d049be310fd5d794eedf9383ab7875da9a9

            <?php else: ?>
             <li><a class="dropdown-button"  data-activates="dropdown-login">Login<i class="material-icons right">arrow_drop_down</i></a>
              <!-- Dropdown Structure -->
              <ul id="dropdown-login" class="dropdown-content">
                <li><a href="/student/login">Student</a></li>
                <li><a href="/startup/login">Startup</a></li>
              </ul>
            </li>
            <li><a class="dropdown-button"  data-activates="dropdown-signup">Signup<i class="material-icons right">arrow_drop_down</i></a>
              <!-- Dropdown Structure -->
              <ul id="dropdown-signup" class="dropdown-content">
                <li><a href="/student/signup">Student</a></li>
                <li><a href="/startup/signup">Startup</a></li>            
              </ul>
            </li>
          <?php endif ?>
         
        </ul>

        <ul id="slide-out" class="side-nav">   
          <li><a href="#"><i class="material-icons left">&#xE915;</i>ABOUT</a></li>
          <li><a href="internships.html"><i class="material-icons left">&#xE168;</i>INTERNSHIPS</a></li>
          <li><a href="student_signup.html"><i class="material-icons left">&#xE151;</i>CV GENERATOR</a></li> 
          <li><a class="dropdown-button"  data-activates="dropdown1">SIGNUP<i class="material-icons right">arrow_drop_down</i></a></li>             
          <li><a href="#"><i class="material-icons left">&#xE8AC;</i>CONTACT</a></li>
        </ul>
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>

      </div>
    </nav>
    <?php if(isset($msg) && !empty($msg)) :?>
      <div class="container">
        <div class="row">
          <div class="col m6 offset-m3 center s8 offset-s2">
            <?php $messages = explode(trim($msg),':;:');
            foreach($messages as $message): ?>
            <br />
            <div class="card-panel teal">
              <span class="white-text">
                <?php echo $msg ?>
              </span>
            </div>
            <br />
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>