 <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="/js/jquery-2.2.4.min.js"></script>
      <script type="text/javascript" src="/js/materialize.min.js"></script>
      <script type="text/javascript" src="/js/custom.js"></script>
      <script type="text/javascript">$(".dropdown-button").dropdown();</script>
  	  <?php 
        //to print extra scripts (just import extra scripts)
          if(isset($scriptsF)){
            foreach ($scriptsF as  $script) {?>
                <script src="<?php echo $script ?>"></script>
      <?php }
          }
      ?>
    <!-- For Editing -->
     <script type="text/javascript">
         //to disable all inputs at first
         function disable(){
             $('input:not(.no-disable)').prop('disabled',true);
             $('textarea:not(.no-disable)').prop('disabled',true);
             $('.do-disable').addClass('disabled').prop('disabled' , true);
             toggleStates('cancelButton','editButton');
             $('#submitButton').prop('disabled',true).addClass('disabled');
         }
         function enable(){
             $('input:not(.no-enable)').prop('disabled',false);
             $('textarea:not(.no-enable)').prop('disabled',false);
             toggleStates('editButton','cancelButton');
             $('#submitButton').prop('disabled',false).removeClass('disabled');
         }

         function toggleStates(toggle1,toggle2){
             $('#'+ toggle1).addClass('disabled').on('click' , function(e){
                 e.stopImmediatePropagation();
                 e.stopPropagation();
                 e.preventDefault();
             });
             $('#' + toggle2).removeClass('disabled').unbind('click');
         }
     </script>
     <script type="text/javascript">
        $(".button-collapse").sideNav();
     </script>
    </body>
  </html>