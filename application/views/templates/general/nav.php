       <nav>
        <div class="nav-wrapper cyan darken-3">
          <a class="brand-logo"  href="<?php echo (isset($isLogin) && $isLogin) ? '/student/home' : base_url() ?>"><img src="/img/logo_white.png"></a>
          <ul class="right hide-on-med-and-down">
            <li><a href="<?php echo base_url('#about')?>">About</a></li>
             <li><a href="<?php echo base_url('#contact')?>">Contact</a></li>
            <?php if(isset($isLogin) && $isLogin): //ankit
		              //if($this->session->userdata('user_id')): //frankle
		?>
                <li><a href="/student/cvbuilder">Cv Generator</a></li>
                <li class="dropdown-button dropdown-wide" data-activates="dropdown-interns">Internships <i class="material-icons right">arrow_drop_down</i>
                    <ul id="dropdown-interns" class="dropdown-content">
                        <li><a href="/student/internships">Available Internships</a></li>
                        <li><a href="/student/profile/internships">Applied Internships</a></li>
                    </ul>
                </li>
                <li><a class="dropdown-button dropdown-med"  data-activates="dropdown-profile">Profile <i class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id="dropdown-profile" class="dropdown-content">
                        <li><a href="/student/profile">Edit Profile</a></li>
                        <li><a href="/student/profile/password">Password</a></li>
                    </ul>
                </li>
                <li><a href="/student/logout">Logout</a></li>
            <?php elseif($this->session->userdata('sip_startup_is_logged_in')):?>
              <ul class="right hide-on-med-and-down">

                <li><a href="<?php echo base_url('startup/home'); ?>">STARTUP HOME</a></li> 
                <li><a href="<?php echo base_url('startup/internships'); ?>">INTERNSHIP OPENINGS</a></li>
                <li><a href="<?php echo base_url('startup/contact/admin'); ?>">QUERIES</a></li> 
                <li><a href="<?php echo base_url('logout/startup_logout'); ?>">LOGOUT</a></li>    
              </ul>

            <?php else: ?>
             <li><a class="dropdown-button"  data-activates="dropdown-login">Login<i class="material-icons right">arrow_drop_down</i></a>
              <!-- Dropdown Structure -->
              <ul id="dropdown-login" class="dropdown-content">
                <li><a href="/student/login">Student</a></li>
                <li><a href="/startup/login">Startup</a></li>
              </ul>
            </li>
            <li><a class="dropdown-button"  data-activates="dropdown-signup">Signup<i class="material-icons right">arrow_drop_down</i></a>
              <!-- Dropdown Structure -->
              <ul id="dropdown-signup" class="dropdown-content">
                <li><a href="/student/signup">Student</a></li>
                <li><a href="/startup/signup">Startup</a></li>            
              </ul>
            </li>
          <?php endif ?>
         
        </ul>


        <!-- For Mobile Side Nav -->
        <ul id="slide-out" class="side-nav" style="color:black;">   
         <li><a href="<?php echo base_url('#about')?>">About</a></li>
             <li><a href="<?php echo base_url('#contact')?>">Contact</a></li>
            <?php if(isset($isLogin) && $isLogin): //ankit
                 //if($this->session->userdata('user_id')): //frankle
    ?>
                <li><a href="/student/cvbuilder">Cv Generator</a></li>
                <li class="dropdown-button dropdown-wide" data-activates="dropdown-interns-m"><a href="#">Internships <i class="material-icons right">arrow_drop_down</i></a>
                    <ul id="dropdown-interns-m" class="dropdown-content">
                        <li><a href="/student/internships">Available Internships</a></li>
                        <li><a href="/student/profile/internships">Applied Internships</a></li>
                    </ul>
                </li>
                <li><a class="dropdown-button dropdown-med"  data-activates="dropdown-profile-m">Profile <i class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id="dropdown-profile-m" class="dropdown-content">
                        <li><a href="/student/profile">Edit Profile</a></li>
                        <li><a href="/student/profile/password">Password</a></li>
                    </ul>
                </li>
                <li><a href="/student/logout">Logout</a></li>
            <?php elseif($this->session->userdata('sip_startup_is_logged_in')):?>
              <ul class="right hide-on-med-and-down">

                <li><a href="<?php echo base_url('startup/home'); ?>">STARTUP HOME</a></li> 
                <li><a href="<?php echo base_url('startup/internships'); ?>">INTERNSHIP OPENINGS</a></li>
                <li><a href="<?php echo base_url('startup/contact/admin'); ?>">QUERIES</a></li> 
                <li><a href="<?php echo base_url('logout/startup_logout'); ?>">LOGOUT</a></li>    
              </ul>

            <?php else: ?>
             <li><a class="dropdown-button"  data-activates="dropdown-login-m">Login<i class="material-icons right">arrow_drop_down</i></a>
              <!-- Dropdown Structure -->
              <ul id="dropdown-login-m" class="dropdown-content">
                <li><a href="/student/login">Student</a></li>
                <li><a href="/startup/login">Startup</a></li>
              </ul>
            </li>
            <li><a class="dropdown-button"  data-activates="dropdown-signup-m">Signup<i class="material-icons right">arrow_drop_down</i></a>
              <!-- Dropdown Structure -->
              <ul id="dropdown-signup-m" class="dropdown-content">
                <li><a href="/student/signup">Student</a></li>
                <li><a href="/startup/signup">Startup</a></li>            
              </ul>
            </li>
          <?php endif ?>
        </ul>
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>

      </div>
    </nav>
    <?php if(isset($msg) && !empty($msg)) :?>
      <div class="container">
        <div class="row">
          <div class="col m6 offset-m3 center s8 offset-s2">
            <?php $messages = explode(trim($msg),':;:');
            foreach($messages as $message): ?>
            <br />
            <div class="card-panel teal">
              <span class="white-text">
                <?php echo $msg ?>
              </span>
            </div>
            <br />
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>
