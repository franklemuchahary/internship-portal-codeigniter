       <nav>
          <div class="nav-wrapper cyan darken-3">
              <a class="brand-logo"  href="<?php echo base_url(); ?>"><img src="/img/logo_white.png"></a>
              <ul class="right hide-on-med-and-down">
                  <li><a href="<?php echo base_url('#about')?>">ABOUT</a></li>
                  <li><a href="<?php echo base_url('#contact')?>">CONTACT</a></li>

                <?php if(isset($login) && $login):?>
                  <li><a href="internships.html">INTERNSHIPS</a></li>
                  <li><a href="student_signup.html">CV GENERATOR</a></li>
                <?php else: ?>
                   <li><a class="dropdown-button"  data-activates="dropdown-login">Login<i class="material-icons right">arrow_drop_down</i></a>
                      <!-- Dropdown Structure -->
                      <ul id="dropdown-login" class="dropdown-content">
                        <li><a href="/student/login">Student</a></li>
                        <li><a href="/startup/login">Startup</a></li>            
                      </ul>
                   </li>
                   <li><a class="dropdown-button"  data-activates="dropdown-signup">Signup<i class="material-icons right">arrow_drop_down</i></a>
                      <!-- Dropdown Structure -->
                      <ul id="dropdown-signup" class="dropdown-content">
                        <li><a href="/student/signup">Student</a></li>
                        <li><a href="/startup/signup">Startup</a></li>            
                      </ul>
                   </li>
               <?php endif ?>
                </ul>
            

              <ul id="slide-out" class="side-nav">   
                <li><a href="#"><i class="material-icons left">&#xE915;</i>ABOUT</a></li>                
                <li><a href="internships.html"><i class="material-icons left">&#xE168;</i>INTERNSHIPS</a></li>
                <li><a href="student_signup.html"><i class="material-icons left">&#xE151;</i>CV GENERATOR</a></li> 
                 <li><a class="dropdown-button"  data-activates="dropdown1">SIGNUP<i class="material-icons right">arrow_drop_down</i></a></li>             
                <li><a href="#"><i class="material-icons left">&#xE8AC;</i>CONTACT</a></li>
              </ul>
            <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
          
          </div>
      </nav>