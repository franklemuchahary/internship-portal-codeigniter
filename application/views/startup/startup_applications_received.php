 <div class="container">
  <div class="row"> 

  <?php 
  $tot=0;
  $fin=0;
  foreach($applics as $x):
    ++$tot;
    if($x->status == 6){
      ++$fin;
    }
  endforeach;
  ?>
  

  <h4 style="text-align:center">APPLICATIONS</h4>

  <div class="col s12 m6">
    <div class="card horizontal">
      <div class="card-stacked">
        <div class="card-content">
          <h5><?php echo $tot; ?></h5>
        </div>
        <div class="card-action">
          Application Count
        </div>
      </div>
    </div>
  </div>

  <div class="col s12 m6">
    <div class="card horizontal">
      <div class="card-stacked">
        <div class="card-content">
          <h5><?php echo $fin; ?></h5>
        </div>
        <div class="card-action">
          Final Selections
        </div>
      </div>
    </div>
  </div>

    <table class="centered stiped" style="overflow-x:auto; margin-top: 5%;">
      <thead>
        <tr>
          <th data-field="id">Name</th>
          <th data-field="name">Email</th>
          <th data-field="price">Phone</th>
          <th data-field="price">Profile</th>
          <th data-field="price">Status</th>
          <th data-field="price">More Details</th>
        </tr>
      </thead>

      <tbody>
        <form action="<?php echo base_url('startup/startup_home/update_applications'); ?>" method="post">
        <?php foreach($applics as $row): ?>
        <tr>
          <td><?php echo $row->name; ?></td>
          <td><?php echo $row->email; ?></td>
          <td><?php echo $row->phone; ?></td>
          <td><?php echo $row->profile_name; ?></td>
          <td>
            <input type="hidden" name="student_id[]" value="<?php echo $row->student_id?>"/>
            <input type="hidden" name="profile_id" value="<?php echo $row->profile_id?>"/>
            <select name="status[]" class="browser-default">
              <?php foreach($stats as $r): ?>
              <option value="<?php echo $r->id;?>" <?php if($r->id==$row->status): echo "selected";endif;?>> <?php echo $r->type; ?> </option>
              <?php endforeach; ?>
            </select>

          </td>
          <td><a href=" <?php echo base_url('startup/applications/applicants').'/'.$row->student_id;?> " >See More...</a></td>
        </tr>
      <?php endforeach; ?>

      <?php if(!empty($applics)): ?>
      <div class="center-align" style="margin-top:5%;">
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
        <input class="btn" type="submit" value="Update Status"/>
      </div>
    <?php endif; ?>
      
      </form>
      </tbody>
    </table>

  </div>
</div>
