<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container">

 <div id="formBackground">

 </div>



 <div class="formContainer">

   <div class="col m12 s12 red-text" style="text-align: center;">
    <p>
      <?php 
      echo validation_errors(); 
      if(isset($account_created)): echo $account_created; endif;
      if(isset($error_creating_account)): echo $error_creating_account; endif;
      ?>
    </p>
  </div>
 
  <div class="row">
  <div class="card-panel white-text teal lighten-2 col s6 offset-s3">
    <h4 class="headings" style="text-align: center;">Startup Registration</h4>
  </div>
  </div>

  <form action="<?php echo base_url('startup/signup_login/startup_signup'); ?>" method="post" class="col s12">
    <div class="row">
      <div class="input-field col m6 s12 ">
        <i class="material-icons prefix">account_circle</i>
        <input id="first_name" name="startup_full_name" type="text" class="validate">
        <label for="first_name">Name</label>
      </div>
      <div class="input-field col m6 s12       ">
        <i class="material-icons prefix">email</i>
        <input id="phone" type="text" class="validate" name="startup_phone">
        <label for="phone">Phone Number</label>
      </div>

    </div>
    <div class="row">
      <div class="input-field col m12">
        <i class="material-icons prefix">email</i>
        <input id="email" type="email" class="validate" name="startup_email">
        <label for="email">Email</label>
      </div>

    </div>
    <div class="row">
      <div class="input-field col m6 s12       ">
        <i class="material-icons prefix">email</i>
        <input id="password" type="password" class="validate" name="startup_password">
        <label for="password">Set Password</label>
      </div>

      <div class="input-field col m6 s12       ">
        <i class="material-icons prefix">email</i>
        <input id="password_repeat" type="password" class="validate" name="startup_password_repeat">
        <label for="password_repeat">Confirm Password</label>
      </div>
    </div>

    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />

    <div class = "row">
      <div class="col s12  finally">
        <button class="btn waves-effect waves-light" type="submit" name="action">
          Create Account
          <i class="material-icons right">send</i>
        </button>
      </div>
    </div>

  </form>
</div>



</div> <!-- container -->