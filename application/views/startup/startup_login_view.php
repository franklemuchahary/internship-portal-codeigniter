<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container">

 <div id="formBackground">

 </div>



 <div class="formContainer">

   <div class="col m12 s12 red-text" style="text-align: center;">
    <p>
      <?php 
      echo validation_errors(); 
      if(isset($verification_successful)): echo $verification_successful; endif;
      ?>
    </p>
  </div>

  <div class="col m12 s12 green-text" style="text-align: center;">
    <p>
      <?php 
      if(isset($account_created)): echo $account_created; endif;
      ?>
    </p>
  </div>
 
 <div class="row">
  <div class="card-panel white-text teal lighten-2 col s6 offset-s3">
    <h4 class="headings" style="text-align: center;">Startup Login </h4>
  </div>
</div>

  <form action="<?php echo base_url('startup/signup_login/startup_login_validate'); ?>" method="post" class="col s12">
    <div class="row">
      <div class="input-field col m6 offset-m3">
        <i class="material-icons prefix">email</i>
        <input id="email" type="email" class="validate" name="startup_login_email">
        <label for="email">Email</label>
      </div>

    </div>
    <div class="row">
      <div class="input-field col m6 s12 offset-m3">
        <i class="material-icons prefix">email</i>
        <input id="password" type="password" class="validate" name="startup_login_password">
        <label for="password">Password</label>
      </div>
    </div>
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
    <div class = "row">
      <div class="col s12  finally">
        <button class="btn waves-effect waves-light" type="submit" name="action">
          Login
          <i class="material-icons right">send</i>
        </button>
      </div>
    </div>

  </form>
</div>



</div> <!-- container -->