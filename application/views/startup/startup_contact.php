<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>



<div class="container listing">
  <div class="row"> 


    <div class="col m12 s12 orange-text orange-lighten-3" style="text-align: center;">
      <!--<h6>You can create internship openings here.</h6> 
      <h6>You will start receiving applications once the portal is fully live.</h6>-->
      <p>
        <?php 
        echo validation_errors(); 
        ?>
      </p>
    </div>
    
    <form  action="<?php echo base_url('startup/startup_home/post_new_query')?>" method="post" class="col s12 m12" style="margin-top:40px">

      <h4 style="text-align:center"> POST A COMPLAINT/QUERY </h4>
      <br>

      <div class="input-field col s12 m12 l12">
        <i class="material-icons prefix">&#xE853;</i>
        <input id="icon_prefix" type="text" class="validate" name="startup_comp_sub">
        <label for="icon_prefix">Subject</label>
      </div>

      <div class="input-field col s12 m12">
        <textarea id="textarea1" class="materialize-textarea" style="padding: .5rem 0;" name="startup_comp_msg"></textarea>
        <label for="textarea1">Message</label>
      </div>
     
      <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
     
      <div class="col s12  finally">
        <button class="btn waves-effect waves-light" type="submit" name="action">POST
          <i class="material-icons right">send</i>
        </button>
      </div>
    </form>

  </div>
</div>



<div class="container" style="margin-top: 5%; margin-bottom: 5%;">
  <div class="row"> 

    <h4 style="text-align:center"> Your Past Queries </h4>

    <div style="overflow-x:auto; margin-top: 5%;">
      <table class="bordered striped centered">
        <thead>
          <tr>
            <th data-field="id">Posted Date</th>
            <th data-field="name">Subject</th>
            <th data-field="price">Message</th>
            <th data-field="price">Status</th>
            <th data-field="price">Admin Reply</th>
          </tr>
        </thead>

        <tbody>
        <?php foreach($contact_data as $row): ?> 

          <tr>
            <td><?php echo date("d-m-Y H:i",strtotime($row->entry_time));?></td>
            
            <td>
            <?php echo $row->subject; ?>
            </td>

            <td><?php echo $row->content; ?></td>

            <td>
            <?php
            switch($row->query_status):
              case 1: 
                echo "Pending";
                break;
              case 2:
                echo "Resolved";
                break;
              case 3: 
                echo "Under Process";
                break;
              default:
                echo "Other";
              endswitch;
            ?>
            </td>
            
            <td><?php echo $row->admin_reply;?></td>
          
          </tr>

        <?php endforeach; ?>
        </tbody>
      </table>  
    </div>
    

  </div>
</div>

