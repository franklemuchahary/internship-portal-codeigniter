
<div class="container"> 
     
      <h4 class="headings">CONTACT INFORMATION</h4><br>

        <?php foreach($stud_det as $row): ?>
            <div class="row">
              <div class="input-field col m6 s12">
                <i class="material-icons prefix">account_circle</i>
                <input disabled id="first_name" type="text" class="validate" name="sfname" value="<?php echo $row->fname ?>"  >
                <label for="first_name">First Name</label>
              </div>
              <div class="input-field col m6 s12">
                <input disabled id="last_name" type="text" class="validate" name="slname" value="<?php echo $row->lname; ?>"  >
                <label for="last_name">Last Name</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col m6 s12 ">
              <i class="material-icons prefix">email</i>
                <input disabled id="email" type="email" class="validate" name="semail" value="<?php echo $row->email ?> "  >
                <label for="email">Email</label>
              </div>

              <div class="input-field col m6 s12 ">
                <input disabled id="rollno" type="text" class="validate" name="sroll"   value="<?php echo $row->rollno; ?>"  >
                <label for="rollno">Roll No.</label>
              </div>
            </div>

            <div class="row">
              <div class="input-field col s12">
              <i class="material-icons prefix">person_pin</i>
                <textarea disabled id="address" class="materialize-textarea" name="address"  ><?php echo $row->address; ?></textarea>
                <label for="address">Address</label>
              </div>

              <div class = "input-field col m6 s6 ">
              <i class="material-icons prefix">person_pin</i>
                  <input disabled id = "birthdatePicker" type="text" class="validate" name="sdob" value="<?php echo $row->dob ?>"  >
                  <label for = "birthdatePicker">Date of birth</label>
                </div>
          
              <div class="input-field col m6 s6">
                <i class="material-icons prefix">phone</i>
                <input disabled id="icon_telephone" type="tel" class="validate" name="sphone"   value="<?php echo $row->phone ?>">
                <label for="icon_telephone">Phone number</label>
              </div>
            </div>

            <div id = "education">
                <br><h4 class="headings">EDUCATION</h4><br>
            </div>


            <div id = "10" style="margin-top:25px;">
                <h5 class="headings">10th</h5>
            </div>
            <?php 
            $data10 = json_decode($row->{'10_education'});
            $data12 = json_decode($row->{'12_education'});
            ?>

            <div class="row">
              <div class="input-field col m6 s12">
                <i class="material-icons prefix">library_books</i>
                <input disabled id="school_name10" type="text" class="validate" name="schoolname10" value="<?php echo $data10->school; ?>">
                <label for="school_name10">School Name</label>
              </div>
              <div class="input-field col m6 s12       ">
                <input disabled id="school_board10" type="text" class="validate" name="schoolboard10" value="<?php echo $data10->board; ?>">
                <label for="school_board10">Board</label>
              </div>
            </div>

            <div class="row">
              <div class="input-field col m6 s12       ">
              <i class="material-icons prefix">assignment</i>
                <input disabled id="result10" type="text" class="validate" name="result10" value="<?php echo $data10->result; ?>">
                <label for="result10">Marks(Percentage or CGPA)</label>
              </div>

            <div class="input-field col m6 s12       ">
                <input disabled id="year10" type="text" class="validate" name="year10" value="<?php echo $data10->year; ?>">
                <label for="year10">Passing year</label>
              </div>
            </div>

            <div id = "12">
                <h5 class="headings">12th</h5>
            </div>


            <div class="row">
              <div class="input-field col m6 s12       ">
                <i class="material-icons prefix">library_books</i>
                <input disabled id="school_name12" type="text" class="validate" name="schoolname12" value="<?php echo $data12->school; ?>">
                <label for="school_name12">School Name</label>
              </div>
              <div class="input-field col m6 s12       ">
                <input disabled id="school_board12" type="text" class="validate" name="schoolboard12" value="<?php echo $data12->board; ?>">
                <label for="school_board12">Board</label>
              </div>
            </div>

            <div class="row">
              <div class="input-field col m6 s12       ">
              <i class="material-icons prefix">assignment</i>
                <input disabled id="result12" type="text" class="validate" name="result12" value="<?php echo $data12->result; ?>">
                <label for="result12">Marks(Percentage or CGPA)</label>
              </div>

            <div class="input-field col m6 s12">
                <input disabled id="year12" type="text" class="validate" name="year12" value="<?php echo $data12->year; ?>">
                <label for="year12">Passing year</label>
              </div>
            </div>

            <div id = "12">
                <h5 class="headings">Entrance Exams</h5>
            </div>

            <div class="row">
              <div class="input-field col m6 s12">
              <i class="material-icons prefix">stars</i>
                <input disabled id="jeemainsr" type="text" class="validate" name="jeemainsr" value="<?php echo $row->JEEMAIN_R ?>"/>
                <label for="jeemainsr">Jee Mains Rank (Optional)</label>
              </div>

              <div class = "input-field col m6 s12">
                  <input disabled id = "jeeadvsr" type="text" class="validate" name="jeeadvsr" value="<?php echo $row->JEEMAIN_A ?>">
                  <label for = "jeeadvsr">Jee Advance Rank (Optional)</label>
                </div>
            </div>

           
            <br><h4 class="headings">PROJECTS</h4><br>

            <?php $i = 1; foreach($stud_proj as $r): ?>
            <div id = "projectsContainer">
              
              <div style="margin-top:25px;">
                <h6 class="headings">Project <?php echo $i; $i++; ?></h6>
              </div>

              <div id = "projectOne" class="projects">

                  <div class="row">
                    <div class="input-field col m6 s12">
                      <input disabled id="pname1" type="text" class="validate" name="pname[]" value="<?php echo $r->name; ?>">
                      <label for="pname1">Project Name</label>
                    </div>
                    <div class="input-field col m6 s12">
                      <input disabled id="prole1" type="text" class="validate" name="prole[]" value="<?php echo $r->role ?>">
                      <label for="prole1">Your Role</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col m6 s12">
                      <input disabled id="pyear1" type="text" class="validate" name="pyear[]" value="<?php echo $r->year ?>"></input>
                      <label for="pyear1">Project Year</label>
                    </div>

                    <div class = "input-field col m6 s12       ">
                        <input disabled id = "pduration1" type="text" class="validate" name="pduration[]" value="<?php echo $r->duration ?>">
                        <label for = "pduration1">Duration</label>
                      </div>
                  </div>

                  <div class="row">
                    <div class="input-field col m12 s12">
                      <textarea disabled id="pdesc1" class="materialize-textarea" name="pdesc[]"> <?php echo $r->description ?> </textarea>
                      <label for="pdesc1">Description</label>
                    </div>
                  </div> 
              </div>
          </div>
        <?php endforeach; ?>

          <div id = "technical">
            
            <br><h4 class = "headings">
              TECHNICAL SKILLS
            </h4><br>

            <div class="row">
                    <div class="input-field col s12">
                      <textarea disabled id="techSkills" class="materialize-textarea" name="technical"><?php echo $row->technical_s; ?></textarea>

                    </div>
            </div>

          </div>

          <div id = "nonTechnical">
            <h4 class = "headings">
              NON-TECHNICAL SKILLS
            </h4>

            <div class="row">
                    <div class="input-field col s12">
                      <textarea disabled id="nonTechSkills" class="materialize-textarea" nmae="nontechnical" ><?php echo $row->non_technical_s; ?></textarea>

                    </div>
            </div>

          </div>

          <div id = "interests">
            <h4 class = "headings">
              INTERESTS
            </h4>

            <div class="row">
                    <div class="input-field col s12">
                      <textarea disabled id="interests" class="materialize-textarea" name="interest" > <?php echo $row->interests; ?></textarea>
                
                    </div>
            </div>

          </div>
          <div id = "courses">
            <h4 class = "headings">
              COURSES
            </h4>

            <div class="row">
                    <div class="input-field col s12">
                      <textarea disabled id="courses" class="materialize-textarea" name="courses"><?php echo $row->courses; ?></textarea>

                    </div>
            </div>

          </div>

          <div id = "achieve">
            <h4 class = "headings">
              AWARDS AND ACHIEVEMENTS
            </h4>

            <div class="row">
                    <div class="input-field col s12">
                      <textarea disabled id="achieve" class="materialize-textarea" name="achieve" ><?php echo $row->achievements; ?></textarea>
                      
                    </div>
            </div>
          </div>

          <div id = "cocurr">
            <h4 class = "headings">
              CO-CURRICULAR ACTIVITIES
            </h4>

            <div class="row">
                    <div class="input-field col s12">
                      <textarea disabled id="cocurr" class="materialize-textarea" name="cocurr"><?php echo $row->cc_activity; ?></textarea>

                    </div>
            </div>
          </div>

          <div id = "volunt">
            <h4 class = "headings">
              VOLUNTEER WORK
            </h4>

            <div class="row">
                    <div class="input-field col s12">
                      <textarea disabled id="volunt" class="materialize-textarea" name="volunt"><?php echo $row->volunteer; ?></textarea>
                    </div>
            </div>

          </div>     

          </form>
        </div>
        </div>
      <?php endforeach; ?>
     

      <?php if(isset($viewEdit) && $viewEdit): ?>
        <script>
          //to disable all inputs at first
          function disable(){
            $('input:not(.no-disable)').prop(' ',true);
            $('textarea:not(.no-disable)').prop(' ',true);
            $('.do-disable').addClass('diabled').prop(' ' , true);
          }
          function enable(){
            $('input:not(.no-enable)').prop(' ',false);
            $('textarea:not(.no-enable)').prop(' ',false);
          }
          disable();
        </script>
      <?php endif; ?>