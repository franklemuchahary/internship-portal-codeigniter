<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<nav>
  <div class="nav-wrapper cyan darken-3">
  <a class="brand-logo"  href="#"><img src="<?php echo base_url(); ?>assets/img/logo_white.png"></a>
    <ul class="right hide-on-med-and-down">

     <li><a href="<?php echo base_url('startup/startup_home'); ?>">HOME</a></li> 
     <li><a href="<?php echo base_url('startup/startup_home/add_internship'); ?>">ADD INTERNSHIP OPENINGS</a></li> 
     <li><a href="<?php echo base_url('logout/startup_logout'); ?>">LOGOUT</a></li>    
   </ul>


   <ul id="slide-out" class="side-nav">   


    <li><a href="<?php echo base_url('startup/startup_home'); ?>"><i class="material-icons left">&#xE8AC;</i>HOME</a></li>
    <li><a href="<?php echo base_url('startup/startup_home/add_internship'); ?>"><i class="material-icons left">&#xE8AC;</i>ADD INTERNSHIP OPENINGS</a></li>
    <li><a href="#"><i class="material-icons left">&#xE8AC;</i>LOGOUT</a></li>
  </ul>
  <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>

</div>
</nav>