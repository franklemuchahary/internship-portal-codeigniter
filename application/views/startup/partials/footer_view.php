  <?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  ?>

  <!--Import jQuery before materialize.js-->
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="/js/materialize.min.js"></script>
  <script type="text/javascript">
	$(document).ready(function(){
		$('select').material_select();
		$(".dropdown-button").dropdown();
    $('.datepicker').pickadate({
      selectMonths: true, // Creates a dropdown to control month
      selectYears: 15 // Creates a dropdown of 15 years to control year
    });
	});
  </script>

   <!-- Date time picker script starts-->
   <script type="text/javascript" src="/libs/datepicker.js"></script>
      
   <script type="text/javascript">
    $('.date_time_pick').appendDtpicker({
    });
   </script>


</body>
</html>