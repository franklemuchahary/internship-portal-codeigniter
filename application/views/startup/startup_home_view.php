<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container listing">
	<div class="row"> 

		<div class="col m12 s12 red-text" style="text-align: center;">
			<p>
				<?php 
				echo validation_errors(); 
				?>
			</p>
		</div>

		<?php foreach($startup_details as $row): ?>
			
			<form action="<?php echo base_url('startup/startup_home/update_startup_info'); ?>" method="post" enctype="multipart/form-data" class="col s12 m12" style="margin-top:40px" >
				<h4 style="text-align:center"> Your Startup Details </h4>

				<div class="input-field col s12 m12 l12">
					<i class="material-icons prefix">&#xE853;</i>
					<input id="icon_prefix" type="text" class="validate" name="startup_name" value="<?php echo $row->startup_name; ?>">
					<label for="icon_prefix">Startup Name</label>
				</div>

				<div class="input-field col s12 m12">
					<textarea id="textarea1" class="materialize-textarea" style="padding: .5rem 0;" name="startup_about"><?php echo $row->startup_about; ?></textarea>
					<label for="textarea1">About </label>
				</div>
				<div class="input-field col s12 m12 l12">
					<i class="material-icons prefix">&#xE84F;</i>
					<input id="icon_prefix" type="text" class="validate" name="startup_sector" value="<?php echo $row->startup_sector; ?>">
					<label for="icon_prefix">Sector</label>
				</div>  
				<div class="input-field col s12 m12 l12">
					<i class="material-icons prefix">&#xE84F;</i>
					<input id="icon_prefix" type="text" class="validate" name="startup_website" value="<?php echo $row->startup_website; ?>">
					<label for="icon_prefix">Website</label>

					<div class="file-field input-field more_db">
						<div class="btn reqcolor">
							<span><i class="material-icons left">&#xE2C3;</i>Upload Logo (Max 3 MB)</span>
							<input type="file" name="startup_logo">
						</div>
						<div class="file-path-wrapper">
							<input class="file-path validate" type="text" value="<?php echo $row->startup_logo;?>">
						</div>
					</div>

					<h4 style="text-align:center">Personal Info ( Will be Kept Private )</h4>
					<div class="input-field col s12 m12 l12">
						<i class="material-icons prefix">&#xE84F;</i>
						<input id="icon_prefix" type="text" class="validate" name="contact_full_name" value="<?php echo $row->startup_full_name; ?>" >
						<label for="icon_prefix">Contact Person</label>
					</div>
					<div class="input-field col s12 m12 l12">
						<i class="material-icons prefix">&#xE84F;</i>
						<input id="icon_prefix" type="text" class="validate" name="startup_phone" value="<?php echo $row->startup_phone; ?>">
						<label for="icon_prefix">Phone Number</label>
					</div>
					<div class="input-field col s12 m12">
						<textarea id="textarea1" class="materialize-textarea" style="padding: .5rem 0;" name="startup_address"><?php echo $row->startup_address; ?></textarea>
						<label for="textarea1">Address </label>
					</div>

					<div class="input-field col s12 m12">
						<input type="checkbox" id="confirm" name="terms_confirm" value="1" required/>
						<label for="confirm">I confirm that the details provided above are correct to the best of my knowledge.</label>						
					</div>
					
				</div>   

				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />

				<div class="col s12  finally">
					<button class="btn waves-effect waves-light" type="submit" name="action">MOVE AHEAD
						<i class="material-icons right">send</i>
					</button>
				</div>

			</form>

		<?php endforeach; ?>

	</div>
</div>