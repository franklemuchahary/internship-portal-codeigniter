<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container" style="margin-top: 5%; margin-bottom: 5%;">
  <div class="row"> 

    <h4 style="text-align:center"> ALL POSTED OPENINGS </h4>

    <div style="overflow-x:auto; margin-top: 5%;">
      <table class="bordered striped centered">
        <thead>
          <tr>
            <th data-field="id">Internship Profile</th>
            <th data-field="name">Internship Type</th>
            <th data-field="price">Stipend</th>
            <th data-field="price">Duration(Weeks)</th>
            <th data-field="price">Application Deadline</th>
            <th data-field="price">View Applications</th>
          </tr>
        </thead>

        <tbody>
        <?php foreach($opening_details as $row): ?> 

          <tr>
            <td><?php echo $row->profile_name;?></td>
            
            <td>
            <?php 
            switch($row->intern_type):
              case 1: 
                echo "Full-Time";
                break;
              case 2: 
                echo "Part-Time";
                break;
              case 3: 
                echo "Campus Ambassador";
                break;
              case 4: 
                echo "Work From Home";
                break;
              default:
                echo "Others";
            endswitch;


            ?>
            </td>

            <td>
            <?php
            switch($row->intern_stipend_type):
              case 1: 
                echo $row->intern_stipend;
                break;
              case 2:
                echo "No Stipend";
                break;
              case 3: 
                echo "Expenses Covered";
                break;
              default:
                echo "Other";
              endswitch;
            ?>
            </td>
            <td><?php echo $row->intern_duration;?></td>
            <td><?php echo $row->intern_deadline;?></td>
            <td><a target="_blank" href="<?php echo base_url('startup/applications').'/'.$row->openings_id; ?>">View</a></td>
          </tr>

        <?php endforeach; ?>
        </tbody>
      </table>  
    </div>
    

  </div>
</div>


<div class="container listing">
  <div class="row"> 
    <div class="col m12 s12 orange-text orange-lighten-3" style="text-align: center;">
      <h6>You can create internship openings here.</h6> 
      <p>
        <?php 
        echo validation_errors(); 
        ?>
      </p>
    </div>

  <?php foreach($startup_details as $sd): ?>
    
    <form  <?php if($sd->terms_confirm!=1): echo 'action="#"'; else: ?> action="<?php echo base_url('startup/startup_home/add_new_internship_opening')?>" method="post" <?php endif;?> class="col s12 m12" style="margin-top:40px">

      <h4 style="text-align:center"> ADD INTERNSHIP OPENING </h4>
      <br>

      <div class="input-field col s12">
        <select name="intern_profile">
          <option value="" disabled selected>Choose Profile</option>
          <?php foreach($intern_profiles as $intp): ?>
          <option value="<?php echo $intp->intern_profile_id; ?>"><?php echo $intp->profile_name; ?></option>
          <?php endforeach;?>
        </select>
        <label>Internship Profile</label>
      </div>
      <div class="input-field col s12">
        <select name="intern_type">
          <option value="" disabled selected>Choose Type</option>
          <option value="1">Full-Time</option>
          <option value="2">Part-Time</option>
          <option value="3">Campus Amabassador</option>
          <option value="4">Work From Home</option>
        </select>
        <label>Internship Type</label>
      </div>

      <div class="input-field col s12 m12">
        <textarea id="textarea1" class="materialize-textarea" style="padding: .5rem 0;" name="intern_descp"></textarea>
        <label for="textarea1">Internship Description</label>
      </div>


      <div class="input-field col s12">
        <select name="intern_stipend_type">
          <option value="" disabled selected>Choose </option>
          <option value="1">Yes</option>
          <option value="2">No</option>
          <option value="3">Expenses Covered</option>
        </select>
        <label>Are you giving a stipend ?</label>
      </div>


      <div class="input-field col s12 m12 l12">
        <i class="material-icons prefix">&#xE853;</i>
        <input id="icon_prefix" type="number" class="validate" name="intern_stipend">
        <label for="icon_prefix">Stipend Amount (Enter 0 If No Stipend)</label>
      </div>

      <div class="input-field col s12 m12 l12">
        <i class="material-icons prefix">&#xE853;</i>
        <input id="icon_prefix" type="text" class="validate" name="intern_duration">
        <label for="icon_prefix">Duration (Weeks)</label>
      </div>

      <div class="input-field col s12 m12 l12">
        <i class="material-icons prefix">&#xE853;</i>
        <input id="icon_prefix" type="text" class="validate" name="intern_location">
        <label for="icon_prefix">Location</label>
      </div>
      <div class="input-field col s12 m12 l12">
        <i class="material-icons prefix">&#xE853;</i>
        <input id="icon_prefix" type="text" class="date_time_pick" name="intern_deadline">
        <label for="icon_prefix">Application Deadline</label>
      </div>
      
      <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
     
      <?php if($sd->terms_confirm!=1): ?>
      <div class="col s12  finally">
        <p class="red-text center-align" style="font-weight: bold;"> Please Update Your <a href="<?php echo base_url('startup/startup_home')?>">Startup Details</a> In Order To Post Internship Openings</p>
      </div>
      <?php else: ?>
      <div class="col s12  finally">
        <button class="btn waves-effect waves-light" type="submit" name="action">POST THIS OPENING
          <i class="material-icons right">send</i>
        </button>
      </div>
      <?php endif;?>
    </form>

  <?php endforeach;?>

  </div>
</div>



