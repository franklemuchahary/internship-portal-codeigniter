
<?php foreach($posts as $post) : ?>
<div class="container listing form-inline">
    <div class="row">
        <div class="card" style="margin-top: 40px">
            <div class="card-image waves-effect waves-block waves-light">
                <img class="activator log" src="<?php echo base_url('uploads/startup_logos/')?><?php echo (!empty($company[$post['startup_id']]['startup_logo'])) ? 
                $company[$post['startup_id']]['startup_logo'] : 'placeholder.jpg' ?>">
            </div>
            <div class="card-content">
                <span class="card-title activator "><?php echo $company[$post['startup_id']]['startup_name']; ?><i class="material-icons right">more_vert</i></span>
                <div class="classy">
                    <?php if(isset($applied) && $applied) : ?>
                        <?php echo form_open('/student/internships/remove',['style' => "display:inline"],['style' => "display:inline"]); ?>
                            <input type="hidden" name="internship_id" value="<?php echo $post['openings_id'];?>"/>
                            <button type="submit" class="waves-effect waves-light btn exbut amber"><i class="material-icons left">thumb_down</i>Remove</button>
                        </form>
                    <?php else : ?>
                        <?php echo form_open('/student/internships'); ?>
                            <input type="hidden" name="internship_id" value="<?php echo $post['openings_id'];?>"/>
                            <button type="submit" class="waves-effect waves-light btn exbut reqcolor"><i class="material-icons left">&#xE8DC;</i>Apply</button>
                        </form>

                    <?php endif; ?>
                    <a class="waves-effect waves-light btn exbut activator"  ><i class="material-icons left">&#xE913;</i><?php echo $post['intern_title'] ?></a>
                    <?php if(isset($applied) && $applied) : ?>
                        <?php
                            switch($post['status_type']){
                                case 'pending' : $color = 'lime darken-3';break;
                                case 'accepted' : $color = ' green darken-4';break;
                                case 'rejected' : $color = 'deep-orange darken-4';break;
                                default : $color = 'brown darken-2';break;
                            }
                        ?>
                        <a class="waves-effect waves-light btn exbut <?php echo $color ?> sti activator">Status : <?php echo $post['status_type'] ?></a>
                    <?php else: ?>
                        <?php if($post['intern_stipend_type'] != 0) : ?>
                            <a class="waves-effect waves-light btn exbut reqcolor sti activator"><i class="material-icons left">&#xE850;</i> &#8377; <?php echo $post['intern_stipend'] ?></a>
                        <?php endif; ?>
                    <?php endif; ?>

                    <!-- <a class="btn-floating btn-large waves-effect waves-light red circul tooltipped" data-position="right" data-delay="50" data-tooltip="Contacts in Database">7692</a> -->
                </div>
            </div>
            <div class="card-reveal">
                <span class="card-title center"><?php echo $company[$post['startup_id']]['startup_name'] ?><i class="material-icons right">close</i></span>
                <p> Description of Profile: <br /><?php echo $post['intern_descp'] ?><br><hr />
                    Job Profile: <?php echo $post['intern_title'] ?><br>
                    Stipend: &#8377; <?php echo $post['intern_stipend'] ?><br>
                    Duration: <?php echo $post['intern_duration'] ?> Months<br />
                    Type of internship: <?php echo $post['intern_type'] ?><br />
                    Location: <?php echo $post['intern_location'] ?><br />
                    Last date to apply:
                <?php echo $post['intern_deadline'] ?>
                </p>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>
<?php if(empty($posts)) : ?>
    <div class="container margin10">
        <div class="row">
            <div class="col m6 offset-m3 s8 offset-s2 center">
                <div class="card-panel teal center-align">
                    <span class="white-text">
                        <?php if(isset($applied) && $applied) : ?>
                            You Haven't Applied to any internships so far! You better Hurry Up !
                        <?php else: ?>
                            There are currently no available internships for your profile. Keep checking the portal since we have recorded this event and
                            we'll come up with startups anyday now.
                        <?php endif; ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
















<!-- Modal Structure -->
<!--<div id="apply" class="modal ">-->
<!--    <div class="modal-content" style="text-align:center">-->
<!--        <h4> Are you sure you want to apply?</h4>-->
<!--        <a class="waves-effect waves-light btn modal-trigger exbut reqcolor" href="#apply"><i class="material-icons left">&#xE8DC;</i>Apply</a>-->
<!--    </div>-->
<!--</div>-->
