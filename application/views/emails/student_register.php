Hey <?php echo $name ?>,
Welcome To Startup Internship Portal by Ecell DTU, powered by REDCARPETUP.<br />
Click on the link below or copy it in your url to activate your account.<br />
<?php $token = urlencode(base64_encode($token)); ?>
<a href="<?php echo base_url('student/activate')."/{$token}" ?>"><?php echo base_url('student/activate')."/{$token}" ?></a>

Keep Rocking,
Ecell DTU Team