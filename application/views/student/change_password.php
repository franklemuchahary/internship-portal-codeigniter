<div class="container listing">
  <div class="row"> 
      <div class="col s12 m12" style="margin-top:40px">
          <?php echo form_open() ?>
        <form action="#"  class="col s12 m12" >
            <div class="input-field col s12 m12 l12">
              <i class="material-icons prefix">&#xE853;</i>
              <input id="icon_prefix" type="password" class="validate" name="old_password" value="<?php echo set_value("old_password") ?>">
              <label for="icon_prefix">Old Password</label>
            </div>
            <div class="input-field col s12 m12 l12">
              <i class="material-icons prefix">&#xE84F;</i>
              <input id="icon_prefix" type="password" class="validate" name="new_password" value="<?php echo set_value("new_password") ?>">
              <label for="icon_prefix">New Password</label>
            </div>
            <div class="input-field col s12 m12 l12">
              <i class="material-icons prefix">&#xE84F;</i>
              <input id="icon_prefix" type="password" class="validate" name="confirm_password" value="<?php echo set_value("confirm_password") ?>">
              <label for="icon_prefix">Confirm New Password</label>
            </div>
            <div class="col s12  finally">
              <button class="btn waves-effect waves-light" type="submit" name="action">Change Password
              <i class="material-icons right">send</i>
              </button>
            </div>
        </form>
      </div>
  </div>
</div>