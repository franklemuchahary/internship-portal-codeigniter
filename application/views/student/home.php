<div class="container">
	<div class="row">
		<div class="col m6 offset-m3 s8 offset-s2">
			<!--<?php
			//if(isset($msg) && !empty($msg)):
			//	$messages = explode($msg,':;:');
				//foreach($messages as $message): ?>
					<div class="card-panel teal">
						<span class="white-text"> <?php //echo $message ?></span>
					</div>
					<br />
				<?php //endforeach; ?>
			<?php //endif; ?> -->
			<br />
			<div class="card-panel teal" style="margin-top: 10%">
				<p style="font-size: 1.2em" class="white-text">
					This is your dashboard. Important Updates and notices will be shown here, you can use the navigation bar to navigate to new internships, applied internships or your CV Builder
				</p>
			</div>
			<br />
			<div class="card-panel blue darken-2" style="margin-top: 1%">
				<p style="font-size: 1.2em;text-align : justify" class="white-text" >
					Application for all the internships are now open. <br />You can apply in any of the internships listed, check your applied internships in (internships->Applied Internships),<br />
					Startups will update your intern application status which is visible in your applied internships listing (internships->Applied Internships) .
				</p>
			</div>
		</div>
	</div>
</div>
