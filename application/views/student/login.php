<div class="container">

     <!-- <div id = "formBackground">
        
      </div> -->

      <div class = "formContainer">
       <div class="row">
  <div class="card-panel white-text blue lighten-2 col s6 offset-s3">
    <h4 class="headings" style="text-align: center;">Student Login</h4>
  </div>
  </div>
        <div class="row">
          <div class="col s12">

              <?php if(isset($errorInvalid)) : ?>
                  <div class="card-panel teal col s6 offset-s3">
                      <span class="white-text">Invalid Username/Password</span>
                  </div>
              <?php endif; ?>

            <?php if(!empty(validation_errors())) : ?>
              <div class="card-panel teal col s6 offset-s3">
                <span class="white-text"><?php echo validation_errors(); ?></span>
              </div>
            <?php endif; ?>
            
            <?php echo form_open('/student/login') ?>
            <div class="row">
              <div class="input-field col m6 offset-m3 s12 center">
                <i class="material-icons prefix">account_circle</i>
                <input id="username" type="text" class="validate" name="username" value="<?php set_value('username') ?>">
                <label for="username">UserName / Email</label>
              </div>            
            </div>
            <div class="row">
              <div class="input-field col m6 offset-m3 s12 center">
              <i class="material-icons prefix">email</i>

                <input id="spassword" type="password" class="validate"  name="spassword">
                <label for="password ">Password</label>
              </div>
            </div>
             <div class = "row">
              <div class="col s12  finally">
                <button class="btn blue lighten-2 waves-effect waves-light" type="submit" name="action">
                  Login
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
            </form>
          </div>
          </div>
        </div>


      </div> <!-- container -->