<div class="container">

    <div id = "formBackground">
        
    </div>
    <div class = "formContainer">
      <div class="row">
  <div class="card-panel white-text blue lighten-2 col s6 offset-s3">
    <h4 class="headings" style="text-align: center;">Student Registration</h4>
  </div>
  </div>
        <div class="row">
          <div class="col s12">

            <?php if(!empty(validation_errors())) : ?>
              <div class="card-panel teal col s6 offset-s3">
                <span class="white-text"><?php echo validation_errors(); ?></span>
              </div>
            <?php endif; ?>
            <?php echo form_open('/student/signup') ?>
            <div class="row">
              <?php if(isset($fail) && $fail === true): ?>
                  <div class="card-panel teal col s6 offset-s3">
                      <span class="white-text">Oops Houston, we encountered some problem registering you, You'll Have to Try Again</span>
                  </div>
              <?php endif; ?>
              <?php if(isset($fail) && $fail === true): ?>
                  <div class="card-panel teal col s6 offset-s3">
                      <span class="white-text">Ehh.. Maybe Try Using your email and you Roll No, because buddy the one's you entered are already taken.<br />
                        Maybe You gotta login instead of signing up again!!!!
                      </span>
                  </div>
              <?php endif; ?>
              <div class="input-field col m6 s12">
                <i class="material-icons prefix">account_circle</i>
                <input id="first_name" type="text" class="validate" name="sname" value="<?php echo set_value('sname') ?>">
                <label for="first_name">Name</label>
              </div>
              <div class="input-field col m6 s12       ">
              <i class="material-icons prefix">email</i>
                <input id="email" type="email" class="validate"  name="semail" value="<?php echo set_value('semail') ?>" >
                <label for="email">Email</label>
              </div>
             
            </div>
            <div class="row">
              <div class="input-field col m6 s12       ">
              <i class="material-icons prefix">email</i>
                <input id="phone" type="number" class="validate"  name="sphone" value="<?php echo set_value('sphone') ?>">
                <label for="phone ">Phone Number</label>
              </div>

              <div class="input-field col m6 s12       ">
              <i class="material-icons prefix">email</i>
                <input id="rollno" type="text" class="validate" name="sroll" value="<?php echo set_value('sroll') ?>">
                <label for="rollno">DTU Roll No.</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col m6 s12       ">
              <i class="material-icons prefix">email</i>
                <input id="password" type="password" class="validate"  name="spassword" value="<?php echo set_value('spassword') ?>">
                <label for="password">Set Password</label>
              </div>

              <div class="input-field col m6 s12       ">
              <i class="material-icons prefix">email</i>

                <input id="srepassword" type="password" class="validate"  name="srepassword" value="<?php echo set_value('srepassword') ?>">
                <label for="srepassword">Confirm Password</label>
              </div>
            </div>
             <div class = "row">
              <div class="col s12  finally">
                <button class="btn blue lighten-2 waves-effect waves-light" type="submit" name="action" value="Submit">
                  Register<i class="material-icons right">send</i>
                </button>
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
    </div> <!-- container -->
       <!--<img src="/img/studentlogin.PNG" class="stu">-->