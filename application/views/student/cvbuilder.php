<?php 
      if(isset($viewEdit) && $viewEdit){
          $formLink = '/student/cvbuilder/update';
          $submit = 'Save';
      }else{
          $formLink = '/student/cvbuilder/';
          $submit = 'Submit';
      }
?>
<div class="container"> 
     
      <h4 class="headings">CONTACT INFORMATION</h4>
          <div class="col s12">
              <br />
              <?php if(!empty(validation_errors())) : ?>
                  <br />
                  <div class="container">
                      <div class="row">
                          <div class="col m8 offset-m2 center s8 offset-s2">
                              <div class="card-panel teal">
                                <span class="white-text">
                                    <?php echo validation_errors() ?>
                                </span>
                              </div>
                          </div>
                      </div>
                  </div>
              <?php endif; ?>

              <?php if(isset($fail) && $fail) : ?>
                  <br />
                  <div class="container">
                      <div class="row">
                          <div class="col m8 offset-m2 center s8 offset-s2">
                              <div class="card-panel teal">
                                <span class="white-text">
                                    There was a problem uploading your C.V. in our database. Please Try Again. If the prblem persists Contact Us.
                                </span>
                              </div>
                          </div>
                      </div>
                  </div>
              <?php endif; ?>
            <?php echo form_open($formLink) ?>
            <div class='row'>
              <?php if(isset($viewEdit) && $viewEdit) : ?>
              <div class="col m6 right-align offset-m6 s8 offset-s4">
                <span class="waves-effect waves-light btn-large" id="editButton" onclick="enable();"><i class="material-icons left"></i>Edit</span>
                <span class="waves-effect waves-light btn-large disabled" id="cancelButton" href="/student/cvbuilder/update" onclick="disable();"><i class="material-icons left"></i>Cancel Edit</span>
              </div>
              <?php endif; ?>
              <?php if(isset($fail) && $fail) : ?>
                <div class="col m6 offset-m3">
                  <div class="card-panel teal">
                    <span class="white-text"> <?php echo validation_errors(); ?>
                    </span>
                  </div>
                </div>
              <?php endif; ?>
            </div>
            <div class="row">
              <div class="input-field col m6 s12">
                <i class="material-icons prefix">account_circle</i>
                <input id="first_name" type="text" class="validate" name="sfname" value="<?php echo set_value('sfname') ?>">
                <label for="first_name">First Name</label>
              </div>
              <div class="input-field col m6 s12       ">
                <input id="last_name" type="text" class="validate" name="slname" value="<?php echo set_value('slname'); ?>">
                <label for="last_name">Last Name</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col m6 s12       ">
              <i class="material-icons prefix">email</i>
                <input id="email" type="email" class="validate no-enable" name="semail" value="<?php echo $user_email; ?> " disabled>
                <label for="email">Email</label>
              </div>

              <div class="input-field col m6 s12       ">
                <input id="rollno" type="text" class="validate no-enable" name="sroll" disabled value="<?php echo $user_roll; ?>">
                <label for="rollno">Roll No.</label>
              </div>
            </div>

            <div class="row">
              <div class="input-field col s12">
              <i class="material-icons prefix">person_pin</i>
                <textarea id="address" class="materialize-textarea" name="address" ><?php echo set_value('address') ?></textarea>
                <label for="address">Address</label>
              </div>

              <div class = "input-field col m6 s12       ">
                  <input id = "birthdatePicker" type="text" class="validate" name="sdob" value="<?php echo set_value('sdob') ?>">
                  <label for = "birthdatePicker">Date of birth (Format : dd-mm-yyyy)</label>
                </div>
            </div>

            <div class = "row" id = "phoneNumber">
              <div class="input-field col m6 s12       ">
                <i class="material-icons prefix">phone</i>
                <input id="icon_telephone" type="tel" class="validate no-enable" name="sphone" disabled value="<?php echo $user_phone ?>">
                <label for="icon_telephone">Phone number</label>
              </div>
            </div>

            <div id = "education">
                <h4 class="headings">EDUCATION</h4>
            </div>


            <div id = "10" style="margin-top:25px;">
                <h5 class="headings">10th</h5>
            </div>


            <div class="row">
              <div class="input-field col m6 s12       ">
                <i class="material-icons prefix">library_books</i>
                <input id="school_name10" type="text" class="validate" name="schoolname10" value="<?php echo set_value('schoolname10') ?>">
                <label for="school_name10">School Name</label>
              </div>
              <div class="input-field col m6 s12       ">
                <input id="school_board10" type="text" class="validate" name="schoolboard10" value="<?php echo set_value('schoolboard10') ?>">
                <label for="school_board10">Board</label>
              </div>
            </div>

            <div class="row">
              <div class="input-field col m6 s12       ">
              <i class="material-icons prefix">assignment</i>
                <input id="result10" type="text" class="validate" name="result10" value="<?php echo set_value('result10') ?>">
                <label for="result10">Marks(Percentage or CGPA)</label>
              </div>

            <div class="input-field col m6 s12       ">
                <input id="year10" type="text" class="validate" name="year10" value="<?php echo set_value('year10') ?>">
                <label for="year10">Passing year</label>
              </div>
            </div>

            <div id = "12">
                <h5 class="headings">12th</h5>
            </div>


            <div class="row">
              <div class="input-field col m6 s12       ">
                <i class="material-icons prefix">library_books</i>
                <input id="school_name12" type="text" class="validate" name="schoolname12" value="<?php echo set_value('schoolname12') ?>">
                <label for="school_name12">School Name</label>
              </div>
              <div class="input-field col m6 s12       ">
                <input id="school_board12" type="text" class="validate" name="schoolboard12" value="<?php echo set_value('schoolboard12') ?>">
                <label for="school_board12">Board</label>
              </div>
            </div>

            <div class="row">
              <div class="input-field col m6 s12       ">
              <i class="material-icons prefix">assignment</i>
                <input id="result12" type="text" class="validate" name="result12" value="<?php echo set_value('result12') ?>">
                <label for="result12">Marks(Percentage or CGPA)</label>
              </div>

            <div class="input-field col m6 s12       ">
                <input id="year12" type="text" class="validate" name="year12" value="<?php echo set_value('year12') ?>">
                <label for="year12">Passing year</label>
              </div>
            </div>


            <div class="row">
              <div class="input-field col m6 s12       ">
              <i class="material-icons prefix">stars</i>
                <input id="jeemainsr" type="text" class="materialize-textarea" name="jeemainsr" value="<?php echo set_value('jeemainsr') ?>"/>
                <label for="jeemainsr">Jee Mains Rank (Optional)</label>
              </div>

              <div class = "input-field col m6 s12">
                  <input id = "jeeadvsr" type="text" class="validate" name="jeeadvsr" value="<?php echo set_value('jeeadvsr') ?>">
                  <label for = "jeeadvsr">Jee Advance Rank (Optional)</label>
                </div>
            </div>

           
            <h4 class="headings">
              PROJECTS
            </h4>
            <div id = "projectsContainer">

              <div id = "projectOne" class="projects">

                  <div class="row">
                    <div class="input-field col m6 s12">
                      <i class="material-icons prefix">library_books</i>
                      <input id="pname1" type="text" class="validate" name="pname[]" value="<?php
                        $value = set_value('pname[]');
                        if(is_array($value)){ echo array_shift($value);} else{ echo $value; } ?>">
                      <label for="pname1">Project Name</label>
                    </div>
                    <div class="input-field col m6 s12">
                      <input id="prole1" type="text" class="validate" name="prole[]" value="<?php
                        $value = set_value('prole[]');
                        if(is_array($value)){
                            echo array_shift($value);
                        }else{
                            echo $value;
                        } ?>">
                      <label for="prole1">Your Role</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col m6 s12">
                    <i class="material-icons prefix">stars</i>
                      <input id="pyear1" type="number" class="validate" name="pyear[]" value="<?php
                        $value = set_value('pyear[]');
                      if(is_array($value)){
                          echo array_shift($value);
                      }else{
                          echo $value;
                      } ?>" >
                      <label for="pyear1">Project Year</label>
                    </div>

                    <div class = "input-field col m6 s12       ">
                        <input id = "pduration1" type="text" class="validate" name="pduration[]" value="<?php
                        $value = set_value('pduration[]');
                        if(is_array($value)){ echo array_shift($value);} else{ echo $value; } ?>">
                        <label for = "pduration1">Duration</label>
                      </div>
                  </div>

                  <div class="row">
                    <div class="input-field col m6 offset-m3 s12">
                      <textarea id="pdesc1" class="materialize-textarea" name="pdesc[]"><?php
                          $value = set_value('pdesc[]');
                          if(is_array($value)) echo array_shift($value); else echo $value;  ?></textarea>
                      <label for="pdesc1">Description</label>
                    </div>
                  </div> 
              </div>
              <?php if((isset($viewEdit) && $viewEdit) || hasDataInFormArray('pname[]')): ?>
                <?php $i = 2 ?>
                <?php while(hasDataInFormArray('pname[]')) : ?>
                    <div class="projects">

                  <div class="row">
                    <div class="input-field col m6 s12">
                      <i class="material-icons prefix">library_books</i>
                      <input id="pname<?php echo $i ?>" type="text" class="validate" name="pname[]" value="<?php
                      $value = set_value('pname[]');
                      if(is_array($value)){ echo array_shift($value);} else{ echo $value; } ?>">
                      <label for="pname1<?php echo $i ?>">Project Name</label>
                    </div>
                    <div class="input-field col m6 s12">
                      <input id="prole<?php echo $i ?>" type="text" class="validate" name="prole[]" value="<?php
                      $value = set_value('prole[]');
                      if(is_array($value)){
                          echo array_shift($value);
                      }else{
                          echo $value;
                      } ?>">
                      <label for="prole<?php echo $i ?>">Your Role</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col m6 s12">
                    <i class="material-icons prefix">stars</i>
                      <input id="pyear<?php echo $i ?>" class="materialize-textarea" name="pyear[]" value="<?php
                      $value = set_value('pyear[]');
                      if(is_array($value)){
                          echo array_shift($value);
                      }else{
                          echo $value;
                      } ?>">
                      <label for="pyear<?php echo $i ?>">Project Year</label>
                    </div>

                    <div class = "input-field col m6 s12       ">
                        <input id = "pduration<?php echo $i ?>" type="text" class="validate" name="pduration[]" value="<?php
                        $value = set_value('pduration[]');
                        if(is_array($value)){ echo array_shift($value);} else{ echo $value; } ?>">
                        <label for = "pduration<?php echo $i ?>">Duration</label>
                      </div>
                  </div>

                  <div class="row">
                    <div class="input-field col m6 s12">
                      <textarea id="pdesc<?php echo $i ?>" class="materialize-textarea" name="pdesc[]"><?php
                          $value = set_value('pdesc[]');
                          if(is_array($value)) echo array_shift($value); else echo $value;  ?></textarea>
                      <label for="pdesc<?php echo $i ?>">Description</label>
                    </div>
                  </div> 
              </div>
                <?php $i++;  ?>
                <?php endwhile; ?>
              <?php endif; ?>
            <div class = "row">
              <a id = "addProject" style="top:0px;" class="btn-floating btn-small waves-effect waves-light grey addButton" onclick="addProject()"><i class="material-icons">add</i></a>
              <a id = "removeProject" class="btn-floating btn-small waves-effect waves-light red addButton" onclick="removeProject()"><i class="material-icons">remove</i></a>
            </div>
          </div>

          <div id = "technical">
            <h4 class = "headings">
              TECHNICAL SKILLS
            </h4>

            <div class="row">
                    <div class="input-field col s12">
                      <textarea id="techSkills" class="materialize-textarea" name="technical"><?php echo set_value('technical') ?></textarea>
                      <label for="techSkills">Enter your skills separated by a comma</label>
                    </div>
            </div>

          </div>

          <div id = "nonTechnical">
            <h4 class = "headings">
              NON-TECHNICAL SKILLS
            </h4>

            <div class="row">
                    <div class="input-field col s12">
                      <textarea id="nonTechSkills" class="materialize-textarea" name="nontechnical"><?php echo set_value('nontechnical') ?></textarea>
                      <label for="nonTechSkills">Enter your skills separated by a comma</label>
                    </div>
            </div>

          </div>

          <div id = "interests">
            <h4 class = "headings">
              INTERESTS
            </h4>

            <div class="row">
                    <div class="input-field col s12">
                      <textarea id="interests" class="materialize-textarea" name="interest"><?php echo set_value('interest') ?></textarea>
                      <label for="interests">Enter your interests separated by a comma</label>
                    </div>
            </div>

          </div>
          <div id = "courses">
            <h4 class = "headings">
              COURSES
            </h4>

            <div class="row">
                    <div class="input-field col s12">
                      <textarea id="courses" class="materialize-textarea" name="courses"><?php echo set_value('courses') ?></textarea>
                      <label for="courses">Enter your courses separated by a comma</label>
                    </div>
            </div>

          </div>

          <div id = "achieve">
            <h4 class = "headings">
              AWARDS AND ACHIEVEMENTS
            </h4>

            <div class="row">
                    <div class="input-field col s12">
                      <textarea id="achieve" class="materialize-textarea" name="achieve" ><?php echo set_value('achieve') ?></textarea>
                      <label for="achieve">Enter your achievements and awards separated by a comma</label>
                    </div>
            </div>
          </div>

          <div id = "cocurr">
            <h4 class = "headings">
              CO-CURRICULAR ACTIVITIES
            </h4>

            <div class="row">
                    <div class="input-field col s12">
                      <textarea id="cocurr" class="materialize-textarea" name="cocurr"><?php echo set_value('cocurr') ?></textarea>
                      <label for="cocurr">Enter your co-curricular separated by a comma</label>
                    </div>
            </div>
          </div>

          <div id = "volunt">
            <h4 class = "headings">
              VOLUNTEER WORK
            </h4>

            <div class="row">
                    <div class="input-field col s12">
                      <textarea id="volunt" class="materialize-textarea" name="volunt"><?php echo set_value('volunt') ?></textarea>
                      <label for="volunt">Enter your volunteer work separated by a comma</label>
                    </div>
            </div>

          </div>      

            <div class = "row">
              <div class="col s12  finally">
                <button class="btn waves-effect waves-light do-disable" id="submitButton" type="submit" name="action">
                  <?php echo $submit ?>
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>


          </form>
        </div>
        </div>
      <script type="text/javascript">
      document.getElementById('removeProject').style.display = "none";
      function addProject()
      {
        var projects = $(".projects").length;
        var newProject = $("#projectOne").clone();
        newProject.attr("id",'');
        newProject.find("input").each(function(){
          $(this).attr('value',' ');
          $(this).attr('id','');
          //changeName($(this));
        });
        newProject.find("textarea").each(function(){
          $(this).empty();
          $(this).attr('id','');
          //changeName($(this));
        });
        function changeName(elem){
            console.log(elem);
          var name = elem.attr('name');
          name = name.slice(0,name.length - 1);
          name = name.concat(projects+1);
          elem.attr('name' , name);
        }
        $(".projects:last").after(newProject);
        if($(".projects").length > 1)  {
            $("#removeProject").show();
        }
      }

      function removeProject()
      {
        $(".projects:last").remove();
         if($(".projects").length == 1)  {
            $("#removeProject").hide();
        } 
      }
      </script>

      <?php if(isset($viewEdit) && $viewEdit): ?>
        <script>
          window.onload = function(){
                disable();
          };
        </script>
      <?php endif; ?>