<?php $toggleActivationMail = false; ?>
<div class="container">
    <div class="row">
        <div class="col m6 offset-m3 center s8 offset-s2" style="margin-top : 10%">
            <?php if(isset($msg)) : ?>
            <?php if($msg == "mail fail") : ?>
            <div class="card-panel teal">
                <span class="white-text">Sorry, We Can't send a mail right now. We are looking into this issue, Please try again. If The Issue Persists Contact us.</span>
            </div>
            <?php elseif($msg =="mail success") : ?>
                    <div class="card-panel teal">
                        <span class="white-text">We have sent you another mail. Please check your inbox for further details. (Also check your spam folder if you don't see our mail)</span>
                    </div>
            <?php endif;endif; ?>

            <?php
            //when by mail authentication fails $status = false
            if(isset($status)): ?>
                <?php if(!$status) : ?>
                    <div class="card-panel teal">
                        <span class="white-text">Invalid Token. Try Again, use the link given in your mail or generate the mail again by logging into your account.</span>
                    </div>
                <?php else: ?>
                    <?php $toggleActivationMail = true; ?>
                <div class="card-panel teal">
                    <span class="white-text">Your Account Has Been Activated</span>
                </div>
                <?php endif; ?>
            <?php endif;?>
            <br />
            <?php if($toggleActivationMail) : ?>
                <a class="waves-effect waves-light btn-large" href="/student/login"><i class="material-icons right">cloud</i>Login</a>
            <?php else : ?>
                <a class="waves-effect waves-light btn-large" href="/student/activate/resend/<?php echo $token ?>"><i class="material-icons right">cloud</i>Resend Email Verification </a>
            <?php endif; ?>
        </div>
    </div>
</div>
