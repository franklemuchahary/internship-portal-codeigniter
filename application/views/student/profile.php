<div class = "formContainer">
      <h4 class="headings"> Student Profile</h4>
        <div class="row">
          <div class="row">
            <div class="col s12 m8 offset-m2">
              <div class="card-panel teal">
                <span class="white-text">Changing Your Email Address will result in reverification of your account, as a consequence your account will be temprorarly suspended
                  till you activate it through the activation link send to you</span>
              </div>
            </div>
          </div>
          <div class="col m6 right-align offset-m6 s8 offset-s4">
            <span class="waves-effect waves-light btn-large" id="editButton" onclick="enable();"><i class="material-icons left"></i>Edit</span>
            <span class="waves-effect waves-light btn-large disabled" id="cancelButton" href="/student/cvbuilder/update" onclick="disable();"><i class="material-icons left"></i>Cancel Edit</span>
          </div>
          <?php if(!empty(validation_errors())) : ?>
            <br />
            <div class="container">
              <div class="row">
                <div class="col m8 offset-m2 center s8 offset-s2">
                  <div class="card-panel teal">
                                <span class="white-text">
                                    <?php echo validation_errors() ?>
                                </span>
                  </div>
                </div>
              </div>
            </div>
          <?php endif; ?>
          <div class="col s12">
            <?php echo form_open('/student/profile') ?>
            <div class="row">
              <div class="input-field col m6 s12">
                <i class="material-icons prefix">account_circle</i>
                <input id="first_name" type="text" class="validate" name="sname" value="<?php echo set_value('sname') ?>" disabled>
                <label for="first_name">Name</label>
              </div>
              <div class="input-field col m6 s12">
              <i class="material-icons prefix">email</i>
                <input id="email" type="email" class="validate" name="semail" value="<?php echo set_value('semail') ?>" disabled>
                <label for="email">Email</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col m6 s12">
              <i class="material-icons prefix">email</i>
                <input id="phone" type="number" class="validate" name="sphone" value="<?php echo set_value('sphone') ?>" disabled>
                <label for="phone">Phone Number</label>
              </div>

              <div class="input-field col m6 s12">
              <i class="material-icons prefix">email</i>
                <input id="rollno" type="text" class="validate" name="sroll" value="<?php echo set_value('sroll') ?>" disabled>
                <label for="rollno">DTU Roll No.</label>
              </div>
            </div>
             <div class = "row">
              <div class="col s12  finally">
                <button class="btn waves-effect waves-light do-disable" id="submitButton" type="submit" name="action">
                  Save
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
            </form>
          </div>
          </div>
        </div>
      </div> <!-- container -->

      <script>
        window.onload = function(){
          disable();
        };
      </script>