<?php

class Intern_profiles_list extends MY_Model{
    const ID = 'intern_profile_id';
    CONST PROFILE_NAME = 'profile_name';
    static protected $table  = 'intern_profiles_list';

    public function getProfileTypesByArray($ids){
        return $this->db->where_in(self::ID,$ids)->get(static::$table)->result_array();
    }
}