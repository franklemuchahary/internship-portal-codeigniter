<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_model extends MY_Model {

    //attributes
    const ID = 'id';
    const EMAIL = 'email';
    const SUBJECT = 'subject';
    const BODY = 'body';
    const CREATED = 'created_on';
    const SENTON = 'sent_on';
    const STATUS = 'status';

    const NOTSENT = 0;
    const SENT = 1;

    static protected $table = 'emails';

    protected $saveDataMap = [
        self::EMAIL,
        self::BODY,
        self::SUBJECT
    ];

    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');

    }


    //assuming has data as email and body
    function saveMail($data){
        $prep = $this->assignMap($this->saveDataMap,$data);
        $prep[self::STATUS] = self::NOTSENT;
        $prep[self::CREATED] = $this->getTimeStamp();
        return $this->db->insert(static::$table,$prep);
    }

    function sendMails(){
        $mails = $this->db->limit(10)->where([self::STATUS => self::NOTSENT])->get(static::$table)->result();
        foreach($mails  as $mail){
            try{
                $mailer = $this->getMailer();
                $mailer->Subject = $mail->subject;
                $mailer->Body = $mail->{self::BODY};
                $mailer->AddAddress($mail->{self::EMAIL});
                if($mailer->Send()) {
                    $update = array(self::STATUS=> self::SENT, self::SENTON=>$this->getTimeStamp());
                    $this->db->where(self::ID, $mail->id);
                    $this->db->update(self::$table, $update);
                    continue;
                }else{
                    //TODO : log why mail not sent perhaps?
                }
            }catch(Exception $e){
                //just skip this email
            }finally {
                if(isset($mailer)){
                    unset($mailer);
                }
            }
        }
    }

    protected function getMailer(){
        if(!class_exists('PHPMailer')){
            require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
        }
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth = true; // enabled SMTP authentication
        $mail->SMTPSecure = "tls";  // prefix for secure protocol to connect to the server
        $mail->Host = "xxxxxxxx";      // setting GMail as our SMTP server
        $mail->Port = 587;                   // SMTP port to connect to GMail
        $mail->Username = "xxxxxxxx";  // user email address
        $mail->Password = "xxxxxxxx"          // password in GMail
        $mail->isHTML(true);
        $mail->SetFrom('xxxxxxxx', 'XXXXXXXXXXXXXXXXXXXXX');  //Who is sending 
        return $mail;
    }



}