<?php
//when account not active user logs in and email sent to make it active again
class Student_details extends MY_Model{

	/* Attributes */
	const TABLE_PASSWORD = 'password';
	const FIELD_PASSWORD = 'spassword';
	const TOKEN = 'token';
	const LAST_LOGIN = 'last_login';
	const CREATED_ON = 'created_on';
	const ACTIVE = 'active';
	const ID = 'id';
	const USERTYPE = 'type';
	const NAME = 'name';
	const EMAIL = 'email';
	const PHONE ='phone';
	const ROLL = 'rollno';
	const UPDATED_BY = 'updated_by';

	const USERNAME = 'username';

	/* Constants */
	const DEACTIVATE = 0;
	const ACTIVATED = 1;
	const DISABLED = 2;

	static protected $account_create_subject = 'SIP Account Verification';
	static protected $account_create_view = 'emails/student_register';

	//protected $table = "student_login";

	static protected $table = "student_login";

	protected $attributes = [
		self::ID,
		self::NAME,
		self::EMAIL,
		self::PHONE,
		self::ROLL,
		self::TABLE_PASSWORD,
		self::TOKEN,
		self::USERTYPE,
		self::CREATED_ON,
		self::LAST_LOGIN,
		self::ACTIVE,
		self::UPDATED_BY
	];
	//table_field => field_attribute_name
	protected $create = [
		self::NAME => 'sname',
	    self::PHONE => 'sphone',
	    self::ROLL => 'sroll',
	    self::EMAIL => 'semail'
	];

	protected $secretKey = 'ldkfhdl2309472&^@@{:{>{<{#@E)*&pijfepihfefT#(*EY*)H)#N*DN(@)<D>#OD';
	//if model instantiated with a record store values here
	protected $values = [];

	static protected $relations = [
		'type' => ['model' => 'usertype', 'rel' => [self::USERTYPE => 'id'] ]
	];

	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		//join models
		///TODO : MOVE TO MY_MODEL
		$this->load->model('usertype');
	}

	public function login(){
		$username = $this->input->post(self::USERNAME);
		$password = $this->input->post(self::FIELD_PASSWORD);
		try{
			$row = $this->db->get_where(self::$table,[self::EMAIL => $username])->row();
			if(count($row) === 0){
				//username not found 
				throw new Exception("Invalid Username");
			}
			$verify = password_verify($password,$row->password);
			if(!$verify){
				throw new Exception("invalid Password");
			}
			//TODO : populate the object here?
			$row->{self::TABLE_PASSWORD} = $password;
			$this->setLoginSession($row);
		}catch(Exception $e){
			throw new Exception($e->getMessage()."Invalid Username or Password");
		}
		return true;
	}

	public function getUserByID($id){
		return (object)parent::getById($id)[0];
	}

	public function activateAccount($token){
		$user = $this->db->where([self::TOKEN => $token])->get(static::$table)->row();
		if(count($user) === 0){
			throw new Exception('Invalid Token: No Valid Account Found With This Token');
		}
		//TODO : token activation for an already activated user
		// elseif($user->{self::ACTIVE} === self::ACTIVATED){
		// 	return true;
		// }
		
		//since we got the user activate him
		$data[self::TOKEN] = $this->getToken(time().$user->email.$token);
		$data[self::ACTIVE] = self::ACTIVATED;
		if(!$this->db->where(self::ID,$user->id)->update(static::$table,$data)){
			throw new Exception('Activation Failed For User');
		}
		return true;
	}

	protected function setLoginSession($user){
		//access session storage and save there
		$this->load->library('auth');
		$this->auth->login($user->email,$user->password);
	}

	public function newStudent(){
		$data = [];
		//populate data from fields
		foreach($this->create as $tableField => $inputField){
			$data[$tableField] = $this->input->post($inputField);
		}
		//create password
		$data[self::TABLE_PASSWORD] = $this->getPassword($this->input->post(self::FIELD_PASSWORD));
		$data[self::TOKEN] = $this->getToken($this->input->post(self::USERNAME));
		$data[self::CREATED_ON] = $this->getTimeStamp();
		$data[self::LAST_LOGIN] = $this->getTimeStamp();
		$data[self::ACTIVE] = self::DEACTIVATE;
		$data[self::USERTYPE] = Usertype::NORMAL;
		try{
			//check for already existing usernames and rollnos
			$query = $this->db->where([self::EMAIL => $data[self::EMAIL]])->or_where([self::ROLL => $data[self::ROLL]])->get(self::$table);
			if(count($query->result()) !== 0){
				throw new Exception('already existing username or rollno found');
			}
			$this->db->insert(self::$table,$data);
			$this->sendMail($data[self::EMAIL],self::$account_create_subject,self::$account_create_view,$data);
		}catch(Exception $e){
			//TODO : SHOW SOME ERROR
			if(strstr($e->getMessage(),'already existing') !== false){
				throw new Exception('Username or RollNo Already Exists in the Database');
			}
			return false;
		}
		$this->load->library('auth');
		//log in user -> fetch the query from th database again!
		$this->auth->login($data[self::USERNAME],$this->input->post(self::FIELD_PASSWORD));
		return true;
	}

	public function updateProfileByUser($user){
		$emailActivation = false;
		//first check if email and roll no are unique or not
		if($user[self::EMAIL] != $this->input->post('semail')){
			$emails = $this->db->limit(1,0)->where([self::EMAIL=>$this->input->post('semail') , self::ID.' != ' => $user[self::ID]])->get(self::$table)->row();
			if(count($emails) !== 0){
				throw new Exception('Already Existing email in The Database found');
			}
		}
		if($user[self::ROLL] != $this->input->post('sroll')){
			$rollno = $this->db->limit(1,0)->where([self::ROLL=>$this->input->post('sroll') , self::ID.' != ' => $user[self::ID]])->get(self::$table)->row();
			if(count($rollno) !== 0){
				throw new Exception('Already Existing rollno in The Database found');
			}
		}
		//update first
		$data = $this->getFromInput($this->create);
		//get the user and check if the email is same or not
		if(trim($this->input->post('semail')) != $user[self::EMAIL]){
			//email activation needed as well
			$emailActivation = true;
			$token = $this->getToken($user[self::EMAIL].$this->input->post('semail').$user[self::PHONE]);
			$data[self::ACTIVE] = self::DEACTIVATE;
			$data[self::TOKEN] = $token;
		}
		$success = $this->db->where(self::ID,$user[self::ID])->update(self::$table,$data);
		if(!$success){
			throw new Exception('Update Operation Failed');
		}
		if(!$emailActivation){
			return;
		}
		$this->sendMail($data[self::EMAIL],self::$account_create_subject,self::$account_create_view,$data);
	}

	/**
	* TODO : what if throw error
	*/
	public function resendMailForUser($token){
		$user = (array)$this->db->where([self::TOKEN => $token])->get(self::$table)->row();
		if(empty($user)){
			return false;
		}
		$this->sendMail($user[self::EMAIL],self::$account_create_subject,self::$account_create_view,$user);
		return true;
	}

	public function changePassword($user){
		if(!password_verify($this->input->post('old_password'),$user->password)){
			throw new Exception('Incorrect Old Password');
		}
		$this->db->where([self::ID => $user->id])
				 ->update(self::$table,
				 	[self::TABLE_PASSWORD => $this->getPassword($this->input->post('new_password'))]
				   );
	}
	private function getPassword($password){
		//if password has null character or \n
		$password = str_replace(["\n","\0"], "", $password);
		$password = password_hash($password,PASSWORD_BCRYPT);
		if(empty($password)){
			throw new Exception("Error : Password Contains Some Off Characters");
		}
		return $password;
	}

	private function getToken($init){
		//token length - 64
		$token = hash('sha256' , time().$this->secretKey.$init.$this->secretKey.time());
		$token = md5($token);
		return $token;
	}

	public function getEscapedToken($token){
		return urlencode(base64_encode($this->getToken($token)));
	}

	public function getTableName(){
		return self::$table;
	}
}