<?php

class Resume extends MY_Model{

	const STUDENTID = 'student_id';
	const FNAME = "fname";
	const LNAME = 'lname';
	const ADDRESS = "address";
	const DOB = 'dob';
	const EDU10 = '10_education';
	const EDU12 = '12_education';
	const JEEM = 'JEEMAIN_R';
	const JEEA = 'JEEMAIN_A';
	const TECHNICAL = 'technical_s';
	const NONTECHNICAL = 'non_technical_s';
	const INTERESTS = 'interests';
	const COURSES = 'courses';
	const ACHIEVEMENTS = 'achievements';
	const COCURR = 'cc_activity';
	const VOLUNT = 'volunteer';
	const CREATEDON = 'created_on';

	protected $mapFields = [
		self::FNAME => 'sfname',
		self::LNAME => 'slname',
		self::ADDRESS => 'address',
		self::DOB => 'sdob',
		self::JEEM => 'jeemainsr',
		self::JEEA => 'jeeadvsr',
		self::TECHNICAL => 'technical',
		self::NONTECHNICAL => 'nontechnical',
		self::INTERESTS => 'interest',
		self::COURSES => 'courses',
		self::ACHIEVEMENTS => 'achieve',
		self::COCURR => 'cocurr',
		self::VOLUNT => 'volunt'
	];

	protected $classMapFor = [
		self::EDU10,
		self::EDU12
	];

	protected $map10Class = [
		'school' => 'schoolname10',
		'board' => 'schoolboard10',
		'result' => 'result10',
		'year' => 'year10'
	];

	protected $map12Class = [
		'school' => 'schoolname12',
		'board' => 'schoolboard12',
		'result' => 'result12',
		'year' => 'year12'
	];

	protected $mapProject = [
		'pname',
		'prole',
		'pdesc',
		'pyear',
		'pduration'
	];

	//projects mapped differently
	//10,12 education's json objects created and stored!

	static protected $table = 'student_resume';

	static protected $relations = [
		'student' => ['model' => 'student/student_details' , 'rel' => [self::STUDENTID => 'id']]
	];
	/**
	*   True -  projects in a single array -> an array of projects
	*   False - arrange projects by field name [] with array of data of that field from each project
	*/
	public function getResumeByUserId($id,$projectsInArray = true){
		if(!is_numeric($id)){
			return [];
		}
		$query = $this->db->where([self::STUDENTID => $id])->get(self::$table)->row_array();
		if(count($query) == 0){
			//todo : log error here
			return [];
		}
		$this->convertIntoMap($this->mapFields,$query);
		//get 10th & 12th Class information
		foreach($this->classMapFor as $field){
			$query = array_merge($this->decodeClassInfo($field,$query[$field]),$query);
			unset($query[$field]);
		}
		//get projects related to this resume
		$this->load->model('student/project');
		if($projectsInArray){
			$query['projects'] = $this->project->getProjectsById($id);
		}else{
			$query = array_merge($query,$this->project->getProjectsById($id,true));
		}
		return $query;
	}

	protected function decodeClassInfo($classType,$classInfo){
		$classInfo = json_decode($classInfo,true);
		if(empty($classInfo)){
			//TODO : log perhaps for which class data invalid
			throw new Exception("Invalid Json Class Data");
		}
		switch($classType){
			case self::EDU10 : $mapType = "map10Class";break;
			case self::EDU12 : $mapType = "map12Class";break;
		}
		$this->convertIntoMap($this->$mapType,$classInfo);
		return $classInfo;
	}

	public function addResumeForUserId($userId){
		$data = $this->extractResumeFromInput($userId);
		$data[self::STUDENTID] = $userId;
		try{
			$this->db->insert(static::$table,$data);

			//get projects now
			$projects = $this->projectsFromInput();
			$this->load->model('student/project');
			$this->project->insertProjects($projects,$userId);
		}catch(Exception $e){
			//TODO : log exception here why not?
			//perform post processing
			throw $e;
		}
	}

	protected function extractResumeFromInput(){
		$data = $this->getFromInput($this->mapFields);
		$data[self::DOB] = $this->getTimeStamp(strtotime($data[self::DOB]));
		//extracting 10,12 info
		$data = array_merge($this->extractClassInfo(),$data);
		$data[self::CREATEDON] = $this->getTimeStamp();
		return $data;
	}

	public function updateResumeForId($userId){
		$data = $this->extractResumeFromInput();
		try{
			$this->db
					->where(self::STUDENTID,$userId)
					->update(static::$table,$data);
			//updated relatedprojects now
			$projects = $this->projectsFromInput();
			if(count($projects) !== 0){
				$this->load->model('student/project');
				$this->project->updateProjects($projects,$userId);
			}
		}catch(Exceptin $e){
			//log why failed and show error
			throw $e;
		}
	}
	protected function projectsFromInput(){
		$projects = [];
		foreach($this->mapProject as $field){
			$$field = $this->input->post($field.'[]');
		}
		$length = max(count(${$this->mapProject[0]}),count(${$this->mapProject[1]}),count(${$this->mapProject[2]}),count(${$this->mapProject[3]}),count(${$this->mapProject[4]}));
		$projects = [];
		for($i =0 ; $i < $length; $i++){
			$curproject = [
				'name' => ${$this->mapProject[0]}[$i],
				'role' => ${$this->mapProject[1]}[$i],
				'desc' => ${$this->mapProject[2]}[$i],
				'year' => $this->getTimeStampWithoutHour($this->getRelativeTimestamp(${$this->mapProject[3]}[$i])),
				'duration' => ${$this->mapProject[4]}[$i],
			];
			//to check if empty project not submitted
			$flag = 0;
			foreach($curproject as $value){
				if(empty(trim($value))){
					$flag++;
				}
			}
			if($flag == 4){
				continue;
			}
			$projects[] = $curproject;
		}
		return $projects;
		// $pname = $this->input->post('pname[]');
		// $prole = $this->input->post('prole[]');
		// $pyear = $this->input->post('pyear[]');
		// $pyear = $this->input->post('pyear[]');
		// $pyear = $this->input->post('pduration[]');
	}

	protected function getRelativeTimestamp($time){
		$time = (int) $time;
		$curTime = date('Y',time());
		$timeDiff = $curTime - $time;
		$positiveSign = ($timeDiff > 0)?true : false;
		$time  = abs($timeDiff*365*24*60*60);
		$time = ($positiveSign) ? (time() - $time ):(time() + $time);
		return $time;
	}

	protected function extractClassInfo(){
		$data = [];
		$data[self::EDU10] = json_encode($this->getFromInput($this->map10Class));
		$data[self::EDU12] = json_encode($this->getFromInput($this->map12Class));
		return $data;
	}
}