<?php
class Project extends MY_Model{
	const STUDENTID = 'student_id';
	const NAME = 'name';
	const ROLE = 'role';
	const YEAR = 'year';
	const DURATION = 'duration';
	const DESCRIPTION = 'description';
	const ID = 'id';
	const CREATEDON = 'created_on';


	static protected $table = 'student_projects';

	protected $mapFields = [
		self::NAME => 'name',
		self::ROLE => 'role',
		self::DESCRIPTION => 'desc',
		self::DURATION => 'duration',
		self::YEAR => 'year'
	];

	protected $mapProjectTable = [
		self::NAME => 'pname[]',
		self::ROLE => 'prole[]',
		self::DESCRIPTION => 'pdesc[]',
		self::YEAR => 'pyear[]',
		self::DURATION => 'pduration[]'
	];

	public function updateProjects($projects,$userId){
		//delete old projects for now and insert new ones
		$this->db->delete(static::$table,[self::STUDENTID => $userId]);
		$this->insertProjects($projects,$userId);
	}

	public function insertProjects($projects,$userId){
		foreach ($projects as $key => &$project) {
			foreach($this->mapFields as $table => $field){
				if(!isset($project[$table])){
					$project[$table] = $project[$field];
					unset($project[$field]);
				}
			}
			//$project[self::YEAR] = strtotime($project[self::YEAR]);
			$project[self::STUDENTID] = $userId;
			$project[self::CREATEDON] = $this->getTimeStamp();
		}
			try{
			$this->db->insert_batch(static::$table,$projects);
		}catch(Exception $e){
			//LOG ERROR HERE!
			throw new Exception("Batch Insertion Operation Failed");
		}
		return true;
	}

	public function getProjectsById($id,$arrangedByMap = false){
		$projects = $this->db->where([self::STUDENTID => $id])->get(self::$table)->result_array();
		foreach($projects as &$project){
			$project[self::YEAR] = strftime("%Y",strtotime($project[self::YEAR]));
		}
		if($arrangedByMap){
			return $this->arrangeProjectDataByMap($projects);
		}else{
			return $projects;
		}
	}

	protected function arrangeProjectDataByMap($projects){
		$mapped = [];
		foreach($this->mapProjectTable as $table => $field){
			foreach($projects as $project){
				if(isset($project[$table])){
					$mapped[$field][] = $project[$table];
				}else{
					//log perhaps since why unmatching?
				}
			}
		}
		return $mapped;
	}
}