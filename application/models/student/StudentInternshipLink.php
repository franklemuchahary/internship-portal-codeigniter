<?php
class StudentInternshipLink extends MY_Model{

	const ID = '';
	const STUDENTID = 'student_id';
	const PROFILEID = 'profile_id';
	const STATUS = 'status';
	const TIME = 'created_on';

	const STATUS_WAITING = 4;
	const STATUS_ACCEPTED = 5;
	const STATUS_REJECTED = 6;

	static protected $table = 'student_internship';

	const SELECT_STATUS = 'status_type';
	//static protected $openingsTable = '';

	protected $map = [
		self::STUDENTID => 'student',
		self::PROFILEID => 'profile'
	];

	static protected $relations = [
		'openings' => [ 'model' => 'startuo_models/posting' , 'rel' => [ self::PROFILEID => 'openings_id'] ]
	];

	public function getAppliedPostingsForId($userId){
		$postings = $this->db->where([self::STUDENTID => $userId])->get(self::$table)->result_array();
		return $postings;
	}

	public function apply($data){
		$data = $this->assignMap($this->map,$data);
		$data[self::STATUS] = self::STATUS_WAITING;
		$data[self::TIME] = $this->getTimeStamp();
		$this->db->insert(self::$table,$data);
	}

	public function getAppliedWithCompany($userId){
		$tableIntern = Posting::getTablename();
		$tableUserType = 'user_type';
		//TODO : maybe not use join here?
		$postings = $this->db->select($tableIntern.'.* , 	'.$tableUserType.".type AS ".self::SELECT_STATUS)->from($tableIntern)
							  ->join(self::$table,$tableIntern.'.openings_id = '.self::$table.'.'.self::PROFILEID)
							  ->join($tableUserType,$tableUserType.".id = ".self::$table.".".self::STATUS)
							  ->where(self::$table.'.'.self::STUDENTID.' = '.$this->db->escape($userId))->get()->result_array();
	    $startups = [];
	    foreach($postings as $post){
	    	$startups[] = $post[Posting::STARTUPID];
	    }
		$postings = $this->openings->populatePostings($postings);
	    $startups = $this->startup->getStartupsByArray($startups);
		$startups = $this->arrangeBy(Startup_main_model::ID,$startups);
		return [$postings,$startups];
	}

	public function remove($userId){
		$post = $this->input->post('internship_id');
		$post = $this->db->where([Posting::ID => $post])->get(Posting::getTablename())->row_array();
		return $this->removeApplication($userId,$post[Posting::ID]);
	}

	protected function removeApplication($userId,$applicationId){
		$data[self::STUDENTID] = $userId;
		$data[self::PROFILEID] = $applicationId;
		$this->db->where($data)->delete(self::$table);
		return true;
	}
}