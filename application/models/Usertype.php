<?php

class Usertype extends CI_Model{
	const NORMAL = 1;
	const ADMIN = 2;
	const SUPER_ADMIN = 3;

	protected $table = 'usertype';
}