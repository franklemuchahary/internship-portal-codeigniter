<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Startup_main_model extends CI_Model {

	const ID = 'id';

	private $session_data;

	static protected $table = 'startups_users';

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
		$this->session_data = $this->session->userdata('sip_startup_is_logged_in');
	}

	public function getStartupsByArray($ids = []){
		return (count($ids) == 0)  ? [] : $this->db->where_in(self::ID,$ids)->get(self::$table)->result_array();
	} 

	function get_startup_details()
	{
		$this->db->where('startup_email', $this->session_data['startup_email']);
		$query = $this->db->get('startups_users');
		return $query->result();	
	}

	function get_startup_and_opening_details()
	{
		$this->db->select('*');
		$this->db->from('intern_openings');
		$this->db->where('startup_id', $this->session_data['startup_id']);
		$this->db->join('startups_users', 'startups_users.id=intern_openings.openings_id');
		$this->db->join('intern_profiles_list', 'intern_profiles_list.intern_profile_id=intern_openings.intern_title');
		$query = $this->db->get();
		return $query->result();	
	}

	function update_startup_data()
	{
		$data_to_update = array(
				'startup_name' => $this->input->post('startup_name',true),
				'startup_about' => $this->input->post('startup_about',true),
				'startup_sector' => $this->input->post('startup_sector',true),
				'startup_website' => $this->input->post('startup_website',true),
				'startup_full_name' => $this->input->post('contact_full_name',true),
				'startup_phone' => $this->input->post('startup_phone',true),
				'startup_address' => $this->input->post('startup_address',true),
				'terms_confirm' => $this->input->post('terms_confirm',true)
			);

		$this->db->where('startup_email', $this->session_data['startup_email']);
		$query = $this->db->update('startups_users', $data_to_update);
		if($query): return true; else: return false; endif;
	}

	function update_logo_file_name($file_name)
	{
		$this->db->where('startup_email', $this->session_data['startup_email']);
		$this->db->where('id', $this->session_data['startup_id']);
		$query = $this->db->update('startups_users', array('startup_logo'=>$file_name));
		if($query): return true; else: return false; endif;
	}


	function get_intern_profiles_list()
	{
		$query = $this->db->get('intern_profiles_list');
		return $query->result();	
	}

	function add_intern_opening_data()
	{
		$data_to_insert = array(
				'startup_id' => $this->session_data['startup_id'],
				'startup_email_username' => $this->session_data['startup_email'],
				'intern_type' => $this->input->post('intern_type',true),
				'intern_title' => $this->input->post('intern_profile',true),
				'intern_descp' => $this->input->post('intern_descp',true),
				'intern_stipend_type' => $this->input->post('intern_stipend_type',true),
				'intern_stipend' => $this->input->post('intern_stipend',true),
				'intern_location' => $this->input->post('intern_location',true),
				'intern_duration' => $this->input->post('intern_duration',true),
				'intern_deadline' => $this->input->post('intern_deadline',true)
			);

		$query = $this->db->insert('intern_openings', $data_to_insert);
		if($query): return true; else: return false; endif;
	}


	function get_applications_data($profile_id)
	{
			
		if($this->_check_profile_id($profile_id) == false):
			throw new exception('Invalid Profile ID');
		else:
			$this->db->select('*');
			$this->db->from('student_internship');
			$this->db->where('profile_id', $profile_id);
			$this->db->join('student_login', 'student_login.id=student_internship.student_id');
			$this->db->join('intern_openings AS io', 'io.openings_id=student_internship.profile_id');
			$this->db->join('intern_profiles_list', 'intern_profiles_list.intern_profile_id=io.intern_title');
			$this->db->join('user_type', 'user_type.id=student_internship.status');
			$query = $this->db->get();
			if($query):
				return $query->result();
			else:
				throw new exception('Invalid Query');
			endif;
		endif;
	}

	function _check_profile_id($id)
	{
		$this->db->where('startup_id', $this->session_data['startup_id']);
		$this->db->where('openings_id', $id);

		$query = $this->db->get('intern_openings');
		return ($query->num_rows()==1) ? true : false;
	}

	function get_status()
	{
		$this->db->limit(5,3);
		$query = $this->db->get('user_type');
		if($query): return $query->result(); else: throw new exception('Invalid'); endif;
	}

	function get_applicant_details($student_id)
	{
		$this->db->select('*');
		$this->db->from('student_login');
		$this->db->where('student_login.id', $student_id);
		$this->db->join('student_resume as sr', 'sr.student_id=student_login.id');
		$query = $this->db->get();
		
		if($query->num_rows() == 1):
			return $query->result();
		else:
			throw new exception('Invalid');
		endif;			
	}

	function get_applicant_projects($student_id)
	{
		$this->db->where('student_id', $student_id);
		$query = $this->db->get('student_projects');
		if($query->num_rows() < 4)
		{
			return $query->result();
		}
		else
		{
			throw new exception('Invalid');
		}
	}


	function update_applic_status($id)
	{
		$this->db->trans_start();

		if(!empty($this->input->post('student_id[]')) && !empty($this->input->post('status[]'))):
			foreach($this->input->post('student_id[]') as $key1=>$value1):
				foreach($this->input->post('status[]') as $key2=>$value2):
					if($key1 == $key2){
						$data_to_update = array('status' => $value2);
					
						$this->db->where('profile_id', $id);
						$this->db->where('student_id', $value1);
						$query = $this->db->update('student_internship', $data_to_update);
					}
				endforeach;
			endforeach;
		else:
			throw new exception('Empty Values');			
		endif;

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE){
			throw new exception('Try Again');
		}
		else{
			return true;
		}
	}


	function get_contact_data()
	{
		$this->db->where('startup_id', $this->session_data['startup_id']);
		$query = $this->db->get('comps_queries');
		if($query):
			return $query->result();
		else:
			return false;
		endif;
	}

	function add_new_comp()
	{
		$data_to_insert = array(
				'startup_id'=>$this->session_data['startup_id'],
				'subject'=>$this->input->post('startup_comp_sub', true),
				'content'=>$this->input->post('startup_comp_msg', true),
			);

		$query = $this->db->insert('comps_queries', $data_to_insert);
		return $query ? true: false;
	}

}