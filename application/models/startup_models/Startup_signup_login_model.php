<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Startup_signup_login_model extends CI_Model {

	private $secretKey = 'asldh8329238h2lf2fml2easpfii2498rouBFgipbf":<":<PMS{DVM';

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
	}

	function create_new_startup_user_db()
	{
		$random_verification_hash = $this->getVerificationHash($this->input->post('startup_email', true).$this->input->post('startup_full_name', true));
		$date_created = date("d-m-Y H:i");
		$password_hash = password_hash($this->input->post('startup_password', true), PASSWORD_BCRYPT);
		$this->add_startup_verification_mail_to_queue($this->input->post('startup_email', true), $random_verification_hash);

		$data_to_insert = array(
				'date_created' => $date_created,
				'startup_email' => $this->input->post('startup_email', true),
				'startup_password' => $password_hash,
				'hash' => $random_verification_hash,
				'startup_full_name' => $this->input->post('startup_full_name', true),
				'startup_phone' => $this->input->post('startup_phone', true),
			);

		$query = $this->db->insert('startups_users', $data_to_insert);
		if($query): return true; else: return false; endif;
	}

	private function getVerificationHash($token){
		//$hash = md5(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
		$hash = md5(time().$this->secretKey.$token.time());
		return $hash;
	}

	
	private function add_startup_verification_mail_to_queue($startup_email, $random_verification_hash)
	{
		$mail_content = '<html>
        	<head>
        		<title>ECELL Startup Internship Portal Account Activation and E-Mail Verification</title>
        	</head>
        	<body>
        	<h3>ECELL Startup Internship Portal Account Activation and E-Mail Verification</h3>
        	<p>Hello! Welcome to Startup Internship Portal!</p><br>
        	<p>Click on the link below or copy it and then paste it in your browser address bar to verify you E-Mail ID and start using your account: </p><br>
        	'.base_url('startup/signup_login/account_verification').'/'.urlencode($startup_email).'/'.urlencode($random_verification_hash).'<br><br><br><br>
        	<p>With Regards</p>
        	<p>The ECELL DTU Team</p>
        	<br><br>
        	</body>
        	</html>';

        $data_to_insert = array(
        		'to_email' => $startup_email,
        		'mail_content' => $mail_content,
        		'status' => 0, //0 means not sent
        	);

        $query = $this->db->insert('queue_startup_verification_mail', $data_to_insert);
        return true;
	}



	//check if the verification hash matched the initially generated hash
	public function verification_email($email_id, $verification_hash)
	{
		$this->db->where('startup_email', urldecode($email_id));
		$this->db->where('hash', $verification_hash);
		$query = $this->db->get('startups_users');
		if($query->num_rows() != 1){
			return false;
		}
		else{
			return true;
		}
	}
	//check if the email if associated has already been verified to prevent further updating
	public function check_if_already_verified($email_id, $verification_hash)
	{
		$this->db->where('startup_email', urldecode($email_id));
		$this->db->where('verification_hash', $verification_hash);
		$query = $this->db->get('startups_users');
		if($query->num_rows() != 1){
			return true;
		}
		else{
			return false;
		}
	}
	//update the verification_hash field in the database
	public function update_verification_hash($email_id, $verification_hash)
	{
		$data_to_update = array('verification_hash' => $verification_hash);

		$this->db->where('startup_email', urldecode($email_id));
		$this->db->where('hash', $verification_hash);
		$query = $this->db->update('startups_users', $data_to_update);
		if($query){
			return true;
		}
		else{
			return false;
		}
	}



	//login model methods start from here

	//admin_login
	public function validate_startup_login($startup_email, $startup_password)
	{
		$hash = $this->get_hashed_password($startup_email);
		if(isset($hash)):
			$password = password_verify($startup_password, $hash->startup_password);
		endif;

		if(isset($password) && $password == TRUE){
			$this->db->select('startup_email, startup_name, id');
			$this->db->from('startups_users');
			$this->db->where('startup_email', $startup_email);
			$this->db->limit(1);
			$query = $this->db->get();

			if($query):
				return $query->result();
			endif;	
		}
		else{
			return FALSE;
		}	

	}

	private function get_hashed_password($startup_email)
	{
		$this->db->select('startup_password');
		$this->db->from('startups_users');
		$this->db->where('startup_email', $startup_email);
		$this->db->limit(1);
		$query = $this->db->get();

		if($query){
			return $query->row();
		}
		else{
			return FALSE;
		}
	}

	//this method updates last login
	public function update_last_login($startup_email)
	{
		date_default_timezone_set('Asia/Kolkata');
		$data_to_update = array('last_login' => date("Y-m-d H:i:s"));

		$this->db->where('startup_email', $startup_email);
		$query = $this->db->update('startups_users', $data_to_update);
		if($query):
			return true;
		else:
			return false;
		endif;
	}


}