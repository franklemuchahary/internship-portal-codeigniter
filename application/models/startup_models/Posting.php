<?php
class Posting extends MY_Model{

	const ID = 'openings_id';
	const STARTUPID = 'startup_id';
	const EMAIL = 'startup_email_username';
	//denotes -> full time, regular etc.
	const INTERN_TYPE = 'intern_type';
	//denotes -> developer, ui/ux etc.
	const INTERN_TITLE = 'intern_title';
	const STATUS = 'status';

	const STATUS_ACCEPTED = 1;
	const STATUS_DECLINED = 0;

	static protected $table = 'intern_openings';

	static protected $relations = [
		'applied_profiles' => ['model' => 'student/StudentInternshipLink' , 'rel' => [self::ID => 'profile_id'] ],
		'startup' => ['model' => 'startup_models/Startup_main_model', 'rel' => [self::STARTUPID => 'id'] ]
	];
	public function __construct(){
		parent::__construct();
	}

	static public function getTablename(){
		return self::$table;
	}

	public function getAllPostingWithCompany($userId){
		//get all active postings
		$appliedPostings = $this->applied_profiles->getAppliedPostingsForId($userId);
		$exclude = [];
		if(count($appliedPostings) !=0){
			foreach($appliedPostings as $profile){
				$exclude[] = $profile[StudentInternshipLink::PROFILEID];
			}
		}
		$postings = $this->getPostings(true,$exclude);
		$startups = [];
		foreach($postings as $post){
			$startups[] = $post[self::STARTUPID];
		}
		//get startups
		$startups = $this->startup->getStartupsByArray($startups);
		$startups = $this->arrangeBy(Startup_main_model::ID,$startups);
		return [$postings,$startups];
	}
	/**
	* active - true,false,null
	*/
	public function getPostings($active = true,$exclude = []){
		$query = $this->db;
		if($active === true){
			$query = $query->where([self::STATUS => self::STATUS_ACCEPTED]);
		}elseif($active === false){
			$query = $query->where([self::STATUS => self::STATUS_DECLINED]);
		}
		if(count($exclude) > 0){
			$query = $query->where_not_in(self::ID,$exclude);
		}
		$postings = $query->get(self::$table)->result_array();

		//interchange interntype with their names
		$this->fillInternTitle($postings);
		$this->fillInternType($postings);
		return $postings;
	}

	public function populatePostings($postings){
		$this->fillInternTitle($postings);
		$this->fillInternType($postings);
		return $postings;
	}

	public function getPostById($id){
		$post = $this->db->where([self::ID => $id])->get(self::$table)->result_array();
		$post  = $this->populatePostings($post);
		return $post;
	}

	protected function fillInternType(&$posts){
		foreach($posts as &$post){
			switch($post[self::INTERN_TYPE]){
				case 0 : $type = "full time";break;
				case 1 : $type = "regular";break;
				default : $type = "not defined";break;
			}
			$post[self::INTERN_TYPE] = $type;
		}
	}

	protected function fillInternTitle(&$posts){
		$titles = [];
		foreach($posts as $post){
			$titles[] = $post[self::INTERN_TITLE];
		}
		if(count($titles) == 0){
			return;
		}
		$this->load->model('intern_profiles_list');
		$types = $this->intern_profiles_list->getProfileTypesByArray($titles);
		$types = $this->arrangeBy(Intern_profiles_list::ID,$types);
		foreach($posts as &$post){
			$post[self::INTERN_TITLE] = $types[$post[self::INTERN_TITLE]][Intern_profiles_list::PROFILE_NAME];
		}
	}

	public function apply($userId){
		$id = $this->getInternshipId();
		$data['student'] = $userId;
		$data['profile'] = $id;
		$this->applied_profiles->apply($data);
	}

	protected function getInternshipId(){
		return $this->input->post('internship_id');
	}
}